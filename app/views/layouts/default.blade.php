<!doctype html>
<html>
    <head>
        <meta charset="utf-8" />
		{{ HTML::style('css/style.css'); }}
    </head>
    
    <body>
        Web Portal for Political Memory
        @yield('content')
    </body>
</html>
