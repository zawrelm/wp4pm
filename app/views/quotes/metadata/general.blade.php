@extends('layouts.default')

@section('content')
    <h1>
        Zitat-Metadaten (ID: {{ $quote->id }}), {{ date("d.m.Y",strtotime($quote->article->date)) }}
    </h1>
    <div><b>{{ $quote->text }}</b></div>
    <hr />
    <div>
        {{ Form::open() }}

        <div title="div_field">
            <h2>Themen</h2>
            {{ link_to("/quotes/{$quote->id}/metadata/field", 'neuer Eintrag') }}
            <div class="div_table_scrollable">
                <table id="table_fields">
                    <th>Name</th>
                    <th>geh&ouml;rt zu</th>
                    <th>ID</th>
                    <th>Themen</th>
					<!--@foreach($quote->fields as $field)
                        @if(!empty($field->parent_field_id))                    
                            <tr class="tr_identified">
                                <td>{{ $field->field->name }}</td>
                                <td>{{ $field->field->parent_field_id }}</td>
                                <td>{{ $field->field->id }}</td>
                                <td>{{ Form::checkbox('field_id[]', $field->field->id, false, ['alt' => $field->field->id]) }}</td>
                            </tr>
                        @endif                                                        
                    @endforeach-->
                    @foreach($fields as $field)
						@if($field->parent_field_id !== NULL)
							<tr class="tr_unidentified">
								<td>{{ $field->name }}</td>
								<td>{{ @Field::find($field->parent_field_id)->name }}</td>
								<td>{{ $field->id }}</td>
								<td>{{ Form::checkbox('field_id[]', $field->id, false, ['alt' => $field->id]) }}</td>
							</tr>
						@endif
                    @endforeach
                </table>
            </div>
        </div>

        <div title="div_politician">
            <h2>Politiker</h2>
            {{ link_to("/quotes/{$quote->id}/metadata/politician", 'neuer Eintrag') }}
            <div class="div_table_scrollable">
                <table id="table_politicians">
                    <th>Nachname</th>
                    <th>Vorname</th>
                    <th>Titel</th>
                    <th>ID</th>
                    <th>Sprecher</th>
                    @foreach($quote->politicians as $politician)
                        <tr class="tr_identified">
                            <td>{{ $politician->politician->lastname }}</td>
                            <td>{{ $politician->politician->firstname }}</td>
                            <td>{{ $politician->politician->title }}</td>
                            <td>{{ $politician->politician->id }}</td>
                            <td>{{ Form::radio('politician_id', $politician->politician->id, false, ['alt' => $politician->politician->id]) }}</td>
                        </tr>
                    @endforeach
                    @foreach($politicians as $politician)
                        <tr class="tr_unidentified">
                            <td>{{ $politician->lastname }}</td>
                            <td>{{ $politician->firstname }}</td>
                            <td>{{ $politician->title }}</td>
                            <td>{{ $politician->id }}</td>
                            <td>{{ Form::radio('politician_id', $politician->id, false, ['alt' => $politician->id]) }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>

        <div title="div_position">
            <h2>Position</h2>
            {{ link_to("/quotes/{$quote->id}/metadata/position", 'neuer Eintrag') }}
            <div class="div_table_scrollable">
                <table id="table_positions">
                    <th>Name</th>
                    <th>ID</th>
                    <th>Position</th>
                    @foreach($quote->typeOfPositions as $typeOfPosition)
                        <tr class="tr_identified">
                            <td>{{ $typeOfPosition->typeOfPosition->name }}</td>
                            <td>{{ $typeOfPosition->typeOfPosition->id }}</td>
                            <td>{{ Form::radio('type_of_position_id', $typeOfPosition->typeOfPosition->id, false, ['alt' => $typeOfPosition->typeOfPosition->id]) }}</td>
                        </tr>
                    @endforeach
                    @foreach($typeOfPositions as $typeOfPosition)
                        <tr class="tr_unidentified">
                            <td>{{ $typeOfPosition->name }}</td>
                            <td>{{ $typeOfPosition->id }}</td>
                            <td>{{ Form::radio('type_of_position_id', $typeOfPosition->id, false, ['alt' => $typeOfPosition->id]) }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>

        <div title="div_party">
            <h2>Partei</h2>
            {{ link_to("/quotes/{$quote->id}/metadata/party", 'neuer Eintrag') }}
            <div class="div_table_scrollable">
                <table id="table_parties">
                    <th>Kurzname</th>
                    <th>Name</th>
                    <th>Link</th>
                    <th>ID</th>
                    <th>Partei</th>
                    @foreach($quote->parties as $party)
                        <tr class="tr_identified">
                            <td>{{ $party->party->shortname }}</td>
                            <td>{{ $party->party->name }}</td>
                            <td>{{ $party->party->link }}</td>
                            <td>{{ $party->party->id }}</td>
                            <td>{{ Form::radio('party_id', $party->party->id, false, ['alt' => $party->party->id]) }}</td>
                        </tr>
                    @endforeach
                    @foreach($parties as $party)
                        <tr class="tr_unidentified">
                            <td>{{ $party->shortname }}</td>
                            <td>{{ $party->name }}</td>
                            <td>{{ $party->link }}</td>
                            <td>{{ $party->id }}</td>
                            <td>{{ Form::radio('party_id', $party->id, false, ['alt' => $party->id]) }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        
        <div>
            <br />
            {{ Form::submit('Änderungen übernehmen!', ['id' => 'button_submit', 'alt' => 'submit']) }}
        </div>
        {{ Form::close() }}
    </div>
@stop
