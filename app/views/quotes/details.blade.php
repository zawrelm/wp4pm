@extends('layouts.default')

@section('content')
    {{ Form::model($quote, array('method' => 'PUT', 'id' => 'save_button')) }}
        <div id="form_buttons">
            <div>
                <h1>
                    Zitat-Details (ID: {{ $quote->id }}), {{ date("d.m.Y",strtotime($quote->article->date)) }}
                    {{ Form::submit('Änderungen übernehmen!', ['id' => 'button_submit', 'alt' => 'submit']) }} 
                </h1>
            </div>
            <div>
                <div class="score_semantic">
                    Semantik: {{ 100*$quote->semantic_score }} %
                </div>
                <div class="score_community">
                    Community: {{ 100*$quote->community_score }} %
                </div>
                <br />
                
                {{ Form::radio('status', 2, ($quote->status == 2), ['id' => 'radio_accept', 'alt' => 'accept']) }}
                <label for="radio_accept"><img src="{{ URL::to('images/button_accept.png') }}" /></label>
                {{ Form::radio('status', -2, ($quote->status == -2), ['id' => 'radio_reject', 'alt' => 'reject']) }}
                <label for="radio_reject"><img src="{{ URL::to('images/button_reject.png') }}" /></label>
            </div>
        </div>
        <div>
            <hr />
            <div>
                {{ link_to("/quotes/{$quote->id}/metadata/general", 'Thema: ') }}
                <span> @foreach($quote->fields as $field)
                        {{ $field->field->name }},
                    @endforeach </span>
            </div>
            <div>
                {{ link_to("/quotes/{$quote->id}/metadata/general", 'Politiker: ') }}
                <span> @foreach($quote->politicians as $politician)
                        {{ $politician->politician->lastname }} {{ $politician->politician->firstname }},
                    @endforeach </span>
            </div>
            <div>
                {{ link_to("/quotes/{$quote->id}/metadata/general", 'Position/Partei: ') }}
                <span> @foreach($quote->typeOfPositions as $typeOfPosition)
                        {{ $typeOfPosition->typeOfPosition->name }},
                    @endforeach / 
                    @foreach($quote->parties as $party)
                        {{ $party->party->shortname }},
                    @endforeach </span>
            </div>
            <div>
                {{ Form::label('text', 'Zitat: ') }} {{ Form::textarea('text') }}
            </div>
            <div>
                <hr />
                Link: {{ link_to($quote->article->link, $quote->article->link) }}
                <div>
                    <br />
                    Originaltext:
                    {{ $quote->article->title }}
                    {{ $quote->article->text }}
                </div>
            </div>
        </div> 
    {{ Form::close() }}
@stop
