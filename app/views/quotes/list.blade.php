@extends('layouts.default')

@section('content')
    <h1>&Uuml;bersicht</h1>

    <h2>neu identifizierte Zitate ({{count($quotes)}})</h2>
    <table id="table_newquotes">
        <th>ID</th>
        <th>Datum</th>
        <th>%-Sem.</th>
        <th>%-Com.</th>
        <th>Thema</th>
        <th>Person</th>
        <th>Partei</th>
        <th>Position</th>
        <th>Zitat</th>
        @foreach($quotes as $quote)
            <tr>
                @if($quote->status == 1)
                    <td>{{ link_to("/quotes/{$quote->id}", $quote->id) }}</td>
                    <td>{{ date("d.m.Y",strtotime($quote->article->date)) }}</td>
                    <td>{{ $quote->semantic_score }}</td>
                    <td>{{ $quote->community_score }}</td>
                    <td>@foreach($quote->fields as $field)
                        {{ $field->field->name }},
                    @endforeach</td>
                    <td>@foreach($quote->politicians as $politician)
                        {{ $politician->politician->lastname }} {{ $politician->politician->firstname }},
                    @endforeach</td>
                    <td>@foreach($quote->parties as $party)
                        {{ $party->party->shortname }},
                    @endforeach</td>
                    <td>@foreach($quote->typeOfPositions as $typeOfPosition)
                        {{ $typeOfPosition->typeOfPosition->name }}
                    @endforeach</td>
                    <td>{{ $quote->text }}</td>

                @endif
            </tr>
        @endforeach
    </table>
    
    <table id="table_acceptedquotes">
    </table>
@stop
