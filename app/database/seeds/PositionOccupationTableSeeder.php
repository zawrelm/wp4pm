<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 22.08.14
 * Time: 12:01
 */

class PositionOccupationTableSeeder extends Seeder {

    public function run()
    {
        DB::table('position_occupations')->delete();

        PositionOccupation::create(array(
            'politician_id' => 1,
            'position_id' => 2,
            'from' => '2004-07-08'
        ));

        PositionOccupation::create(array(
            'politician_id' => 2,
            'position_id' => 1,
            'from' => '2008-08-08'
        ));

        PositionOccupation::create(array(
            'politician_id' => 2,
            'position_id' => 3,
            'from' => '2008-12-02'
        ));

        PositionOccupation::create(array(
            'politician_id' => 2,
            'position_id' => 5,
            'from' => '2007-01-01',
            'to' => '2008-08-08'
        ));

        PositionOccupation::create(array(
            'politician_id' => 2,
            'position_id' => 11,
            'from' => '1994-01-01',
            'to' => '2007-01-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 2,
            'position_id' => 12,
            'from' => '1985-01-01',
            'to' => '1994-01-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 2,
            'position_id' => 13,
            'from' => '1985-01-01',
            'to' => '1994-01-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 3,
            'position_id' => 1,
            'from' => '2011-01-01',
            'to' => '2014-01-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 3,
            'position_id' => 4,
            'from' => '2011-04-21',
            'to' => '2013-09-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 3,
            'position_id' => 5,
            'from' => '2008-12-02',
            'to' => '2014-09-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 3,
            'position_id' => 8,
            'from' => '2001-01-01',
            'to' => '2001-01-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 3,
            'position_id' => 9,
            'from' => '1992-10-22',
            'to' => '1993-12-16'
        ));

        PositionOccupation::create(array(
            'politician_id' => 3,
            'position_id' => 15,
            'from' => '1995-01-01',
            'to' => '1996-10-29'
        ));

        PositionOccupation::create(array(
            'politician_id' => 4,
            'position_id' => 1,
            'from' => '2005-04-23'
        ));

        PositionOccupation::create(array(
            'politician_id' => 4,
            'position_id' => 8,
            'from' => '2001-01-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 4,
            'position_id' => 12,
            'from' => '1996-01-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 4,
            'position_id' => 14,
            'from' => '2004-03-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 5,
            'position_id' => 1,
            'from' => '2008-01-01',
        ));

        PositionOccupation::create(array(
            'politician_id' => 5,
            'position_id' => 8,
            'from' => '2004-01-01',
        ));

        PositionOccupation::create(array(
            'politician_id' => 6,
            'position_id' => 1,
            'from' => '2004-01-01',
            'to' => '2013-10-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 6,
            'position_id' => 8,
            'from' => '2004-01-01',
            'to' => '2013-09-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 7,
            'position_id' => 1,
            'from' => '2012-01-01',
            'to' => '2013-11-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 7,
            'position_id' => 8,
            'from' => '2013-09-01',
            'to' => '2013-11-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 8,
            'position_id' => 1,
            'from' => '2014-01-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 8,
            'position_id' => 4,
            'from' => '2014-01-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 8,
            'position_id' => 5,
            'from' => '2009-09-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 8,
            'position_id' => 8,
            'from' => '1998-01-01',
            'to' => '2009-09-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 9,
            'position_id' => 1,
            'from' => '2013-11-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 9,
            'position_id' => 8,
            'from' => '2013-10-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 10,
            'position_id' => 7,
            'from' => '1994-01-01'
        ));

        PositionOccupation::create(array(
            'politician_id' => 10,
            'position_id' => 14,
            'from' => '1994-01-01'
        ));

    }
    
}
