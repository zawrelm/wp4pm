<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 21.08.14
 * Time: 17:30
 */

class PartyTagTableSeeder extends Seeder {

    public function run()
    {
        DB::table('party_tags')->delete();

        PartyTag::create(array(
            'tag' => 'SPÖ',
            'party_id' => 1
        ));

        PartyTag::create(array(
            'tag' => 'sozialdemokrat',
            'party_id' => 1
        ));

        PartyTag::create(array(
            'tag' => 'ÖVP',
            'party_id' => 2
        ));

        PartyTag::create(array(
            'tag' => 'Volkspartei',
            'party_id' => 2
        ));

        PartyTag::create(array(
            'tag' => 'FPÖ',
            'party_id' => 3
        ));

        PartyTag::create(array(
            'tag' => 'Freiheitlich',
            'party_id' => 3
        ));

        PartyTag::create(array(
            'tag' => 'Grüne',
            'party_id' => 4
        ));

        PartyTag::create(array(
            'tag' => 'Stronach',
            'party_id' => 5
        ));

        PartyTag::create(array(
            'tag' => 'NEOS',
            'party_id' => 6
        ));

        PartyTag::create(array(
            'tag' => 'BZÖ',
            'party_id' => 7
        ));

        PartyTag::create(array(
            'tag' => 'KPÖ',
            'party_id' => 8
        ));

        PartyTag::create(array(
            'tag' => 'kommunist',
            'party_id' => 8
        ));

        PartyTag::create(array(
            'tag' => 'PPÖ',
            'party_id' => 9
        ));

        PartyTag::create(array(
            'tag' => 'Piraten',
            'party_id' => 9
        ));

        PartyTag::create(array(
            'tag' => 'überparteilich',
            'party_id' => 10
        ));

        PartyTag::create(array(
            'tag' => 'parteilos',
            'party_id' => 11
        ));

        PartyTag::create(array(
            'tag' => 'parteifrei',
            'party_id' => 11
        ));

    }

}
