<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('SourceTableSeeder');
        $this->call('FieldTableSeeder');
        $this->call('FieldTagTableSeeder');
        $this->call('PartyTableSeeder');
        $this->call('PartyTagTableSeeder');
        $this->call('TypeOfPositionTableSeeder');
        $this->call('TypeOfPositionTagTableSeeder');
        $this->call('PoliticianTableSeeder');
        $this->call('PoliticianTagTableSeeder');
        $this->call('PositionTableSeeder');
        $this->call('PositionOccupationTableSeeder');
//        $this->call('OntologyWordSeeder');
	}

}
