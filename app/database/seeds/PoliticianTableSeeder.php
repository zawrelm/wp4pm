<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 22.08.14
 * Time: 10:03
 */

class PoliticianTableSeeder extends Seeder {

    public function run()
    {
        DB::table('politicians')->delete();

        Politician::create(array(
            'firstname' => 'Heinz',
            'lastname' =>  'Fischer',
            'title' => 'Dr.'
            //'link_to_profile' => 'http://www.parlament.gv.at/WWER/PAD_00334/'
        ));

        Politician::create(array(
            'firstname' => 'Werner',
            'lastname' =>  'Faymann',
            'title' => ''
            //'link_to_profile' => 'http://www.parlament.gv.at/WWER/PAD_36450/'
        ));

        Politician::create(array(
            'firstname' => 'Michael',
            'lastname' =>  'Spindelegger',
            'title' => 'Dr.'
            //'link_to_profile' => 'http://www.parlament.gv.at/WWER/PAD_01817/'
        ));

        Politician::create(array(
            'firstname' => 'Heinz-Christian',
            'lastname' =>  'Strache',
            'title' => ''
            //'link_to_profile' => 'http://www.parlament.gv.at/WWER/PAD_35518/'
        ));

        Politician::create(array(
            'firstname' => 'Eva',
            'lastname' =>  'Glawischnig-Piesczek',
            'title' => 'Dr.'
            //'link_to_profile' => 'http://www.parlament.gv.at/WWER/PAD_08240/'
        ));

        Politician::create(array(
            'firstname' => 'Josef',
            'lastname' =>  'Bucher',
            'title' => ''
            //'link_to_profile' => 'http://www.parlament.gv.at/WWER/PAD_14856/'
        ));

        Politician::create(array(
            'firstname' => 'Frank',
            'lastname' =>  'Stronach',
            'title' => ''
            //'link_to_profile' => 'http://www.teamstronach.at/de/frank'
        ));

        Politician::create(array(
            'firstname' => 'Reinhold',
            'lastname' =>  'Mitterlehner',
            'title' => 'Dr.'
            //'link_to_profile' => 'http://www.parlament.gv.at/WWER/PAD_08696/'
        ));

        Politician::create(array(
            'firstname' => 'Kathrin',
            'lastname' =>  'Nachbaur',
            'title' => 'Dr.'
            //'link_to_profile' => 'http://www.parlament.gv.at/WWER/PAD_83139/'
        ));

        Politician::create(array(
            'firstname' => 'Michael',
            'lastname' =>  'Häupl',
            'title' => 'Dr.'
            //'link_to_profile' => 'http://www.wien.gv.at/advuew/internet/AdvPrSrv.asp?Layout=llanzeige&Type=K&PERSONCD=2004073010475502'
        ));

        Politician::create(array(
            'firstname' => 'Matthias',
            'lastname' =>  'Strolz',
            'title' => 'Mag. Dr.'
        ));

        Politician::create(array(
            'firstname' => 'Gabriele',
            'lastname' =>  'Heinisch-Hosek',
            'title' => 'Mag.'
        ));

        Politician::create(array(
            'firstname' => 'Hans Jörg',
            'lastname' =>  'Schelling',
            'title' => 'Dr.'
        ));

        Politician::create(array(
            'firstname' => 'Erwin',
            'lastname' =>  'Pröll',
            'title' => 'Dr.'
        ));

        Politician::create(array(
            'firstname' => 'Hans',
            'lastname' =>  'Niessl',
            'title' => 'Dr.'
        ));

        Politician::create(array(
            'firstname' => 'Wolfgang',
            'lastname' =>  'Brandstetter',
            'title' => 'Dr.'
        ));

        Politician::create(array(
            'firstname' => 'Gerald',
            'lastname' =>  'Klug',
            'title' => 'Mag.'
        ));
		
    }
    
}
