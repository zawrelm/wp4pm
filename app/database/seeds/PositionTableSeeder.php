<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 22.08.14
 * Time: 11:20
 */

class PositionTableSeeder extends Seeder {

    public function run()
    {
        DB::table('positions')->delete();

        Position::create(array(
            'party_id' => 10,
            'type_of_position_id' => 2
        ));

        Position::create(array(
            'party_id' => 1,
            'type_of_position_id' => 1
        ));

        Position::create(array(
            'party_id' => 10,
            'type_of_position_id' => 3
        ));

        Position::create(array(
            'party_id' => 2,
            'type_of_position_id' => 1
        ));

        Position::create(array(
            'party_id' => 10,
            'type_of_position_id' => 4
        ));

        Position::create(array(
            'party_id' => 10,
            'type_of_position_id' => 5
        ));

        Position::create(array(
            'party_id' => 3,
            'type_of_position_id' => 1
        ));

        Position::create(array(
            'party_id' => 3,
            'type_of_position_id' => 8
        ));

        Position::create(array(
            'party_id' => 4,
            'type_of_position_id' => 1
        ));

        Position::create(array(
            'party_id' => 4,
            'type_of_position_id' => 8
        ));

        Position::create(array(
            'party_id' => 7,
            'type_of_position_id' => 1
        ));

        Position::create(array(
            'party_id' => 7,
            'type_of_position_id' => 8
        ));

        Position::create(array(
            'party_id' => 5,
            'type_of_position_id' => 1
        ));

        Position::create(array(
            'party_id' => 2,
            'type_of_position_id' => 1
        ));

        Position::create(array(
            'party_id' => 10,
            'type_of_position_id' => 4
        ));

        Position::create(array(
            'party_id' => 10,
            'type_of_position_id' => 5
        ));

        Position::create(array(
            'party_id' => 5,
            'type_of_position_id' => 1
        ));

        Position::create(array(
            'party_id' => 5,
            'type_of_position_id' => 8
        ));

        Position::create(array(
            'party_id' => 10,
            'type_of_position_id' => 6
        ));

        Position::create(array(
            'party_id' => 10,
            'type_of_position_id' => 7
        ));

        Position::create(array(
            'party_id' => 1,
            'type_of_position_id' => 14
        ));


    }
    
}

