<?php

class SourceTableSeeder extends Seeder {

    public function run()
    {
        DB::table('sources')->delete();

        Source::create(array(
            'name' => 'derstandard.at',
            'link' => 'http://derstandard.at'
        ));

        Source::create(array(
            'name' => 'diepresse.com',
            'link' => 'http://diepresse.com'
        ));

    }

}
 