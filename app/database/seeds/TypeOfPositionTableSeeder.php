<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 22.08.14
 * Time: 10:56
 */

class TypeOfPositionTableSeeder extends Seeder {

    public function run()
    {
        DB::table('type_of_positions')->delete();

        TypeOfPosition::create(array(
            'name' => 'Bundesparteivorsitzender'
        ));

        TypeOfPosition::create(array(
            'name' => 'Bundespräsident'
        ));

        TypeOfPosition::create(array(
            'name' => 'Bundeskanzler'
        ));

        TypeOfPosition::create(array(
            'name' => 'Vizekanzler'
        ));

        TypeOfPosition::create(array(
            'name' => 'Bundesminister'
        ));

        TypeOfPosition::create(array(
            'name' => 'Landeshauptmann'
        ));

        TypeOfPosition::create(array(
            'name' => 'Bürgermeister'
        ));

        TypeOfPosition::create(array(
            'name' => 'Abgeordneter zum Nationalrat'
        ));

        TypeOfPosition::create(array(
            'name' => 'Abgeordneter zum Bundesrat'
        ));

        TypeOfPosition::create(array(
            'name' => 'Präsident einer Interessensvertretung'
        ));

        TypeOfPosition::create(array(
            'name' => 'Vorsitzender einer Behörde'
        ));
        
        TypeOfPosition::create(array(
            'name' => 'Abgeordneter zum Landtag'
        ));

        TypeOfPosition::create(array(
            'name' => 'Gemeinderat'
        ));
        
        TypeOfPosition::create(array(
            'name' => 'Landesparteivorsitzender'
        ));

        TypeOfPosition::create(array(
            'name' => 'Abgeordneter zum Europäischen Parlament'
        ));

    }
    
}
