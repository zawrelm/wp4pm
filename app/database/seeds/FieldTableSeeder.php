<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 21.08.14
 * Time: 18:32
 */

class FieldTableSeeder extends Seeder {

    public function run()
    {
        DB::table('fields')->delete();

    /* Level 1: Main Political Fields (1-15) */

        Field::create(array(
            'name' => 'Arbeit und Wirtschaft'
        ));

        Field::create(array(
            'name' => 'Auswärtige Angelegenheiten'
        ));

        Field::create(array(
            'name' => 'Familie, Partnerschaft und Jugend'
        ));

        Field::create(array(
            'name' => 'Gesundheit'
        ));

        Field::create(array(
            'name' => 'Infrastruktur, Bauen und Wohnen'
        ));

        Field::create(array(
            'name' => 'Innere Angelegenheiten'
        ));

        Field::create(array(
            'name' => 'Justiz und allgemeine Rechtsordnung'
        ));

        Field::create(array(
            'name' => 'Kultur und Medien, Freizeit'
        ));

        Field::create(array(
            'name' => 'Land- und Forstwirtschaft, Umweltschutz'
        ));

        Field::create(array(
            'name' => 'Landesverteidigung'
        ));

        Field::create(array(
            'name' => 'Soziales'
        ));

        Field::create(array(
            'name' => 'Sport'
        ));

        Field::create(array(
            'name' => 'Steuern und Finanzen'
        ));

        Field::create(array(
            'name' => 'Unterricht und Bildung'
        ));

        Field::create(array(
            'name' => 'Wissenschaft und Forschung'
        ));

    /* Level 2: Political Subfields (16-40) */

        Field::create(array(
            'name' => 'Arbeitsrecht, Gleichberechtigung und Frauen',
            'parent_field_id' => 1
        ));

        Field::create(array(
            'name' => 'Dienstleistungen',
            'parent_field_id' => 1
        ));

        Field::create(array(
            'name' => 'Industrie',
            'parent_field_id' => 1
        ));

        Field::create(array(
            'name' => 'Konsumentenschutz',
            'parent_field_id' => 1
        ));

        Field::create(array(
            'name' => 'Tourismus',
            'parent_field_id' => 1
        ));

        Field::create(array(
            'name' => 'Europa',
            'parent_field_id' => 2
        ));

        Field::create(array(
            'name' => 'International',
            'parent_field_id' => 2
        ));

        Field::create(array(
            'name' => 'Migration und Integration',
            'parent_field_id' => 2
        ));

        Field::create(array(
            'name' => 'Ehe und Partnerschaft',
            'parent_field_id' => 3
        ));

        Field::create(array(
            'name' => 'Jugend',
            'parent_field_id' => 3
        ));

        Field::create(array(
            'name' => 'Ärzte',
            'parent_field_id' => 4
        ));

        Field::create(array(
            'name' => 'Krankenkassen',
            'parent_field_id' => 4
        ));

        Field::create(array(
            'name' => 'Spitäler',
            'parent_field_id' => 4
        ));

        Field::create(array(
            'name' => 'Vorsorge',
            'parent_field_id' => 4
        ));

        Field::create(array(
            'name' => 'Bauen und Wohnen',
            'parent_field_id' => 5
        ));

        Field::create(array(
            'name' => 'Energie',
            'parent_field_id' => 5
        ));

        Field::create(array(
            'name' => 'IT und Datenschutz',
            'parent_field_id' => 5
        ));

        Field::create(array(
            'name' => 'Verkehr (ÖPV, Individualverkehr)',
            'parent_field_id' => 5
        ));

        Field::create(array(
            'name' => 'Wasser- und Abfallwirtschaft',
            'parent_field_id' => 5
        ));

        Field::create(array(
            'name' => 'Katastrophenschutz',
            'parent_field_id' => 6
        ));

        Field::create(array(
            'name' => 'Sicherheit und Kriminalität (Polizei und Exekutive, Terrorismusbekämpfung)',
            'parent_field_id' => 6
        ));

        Field::create(array(
            'name' => 'Wahlen',
            'parent_field_id' => 6
        ));

        Field::create(array(
            'name' => 'Privatrecht',
            'parent_field_id' => 7
        ));

        Field::create(array(
            'name' => 'Strafrecht',
            'parent_field_id' => 7
        ));

        Field::create(array(
            'name' => 'Verwaltungsrecht',
            'parent_field_id' => 7
        ));

    /* Level 2: Political Subfields (41-61) */
        Field::create(array(
            'name' => 'Vollzug',
            'parent_field_id' => 7
        ));

        Field::create(array(
            'name' => 'Freizeit',
            'parent_field_id' => 8
        ));

        Field::create(array(
            'name' => 'Kultur',
            'parent_field_id' => 8
        ));

        Field::create(array(
            'name' => 'Medien',
            'parent_field_id' => 8
        ));

        Field::create(array(
            'name' => 'Forstwirtschaft',
            'parent_field_id' => 9
        ));

        Field::create(array(
            'name' => 'Landwirtschaft',
            'parent_field_id' => 9
        ));

        Field::create(array(
            'name' => 'Umwelt',
            'parent_field_id' => 9
        ));

        Field::create(array(
            'name' => 'Bundesheer',
            'parent_field_id' => 10
        ));

        Field::create(array(
            'name' => 'Grenzschutz',
            'parent_field_id' => 10
        ));

        Field::create(array(
            'name' => 'Behinderung',
            'parent_field_id' => 11
        ));

        Field::create(array(
            'name' => 'Pensionisten',
            'parent_field_id' => 11
        ));

        Field::create(array(
            'name' => 'Sozialversicherung',
            'parent_field_id' => 11
        ));

        Field::create(array(
            'name' => 'Allgemein',
            'parent_field_id' => 12
        ));

        Field::create(array(
            'name' => 'Budget',
            'parent_field_id' => 13
        ));

        Field::create(array(
            'name' => 'Steuern',
            'parent_field_id' => 13
        ));

        Field::create(array(
            'name' => 'Sonstiges',
            'parent_field_id' => 13
        ));

        Field::create(array(
            'name' => 'Fortbildung',
            'parent_field_id' => 14
        ));

        Field::create(array(
            'name' => 'Kindergärten',
            'parent_field_id' => 14
        ));

        Field::create(array(
            'name' => 'Schulen (Gesamtschule, LehrerInnenbildung)',
            'parent_field_id' => 14
        ));

        Field::create(array(
            'name' => 'Forschung',
            'parent_field_id' => 15
        ));

        Field::create(array(
            'name' => 'Universitäten',
            'parent_field_id' => 15
        ));

    }

}
