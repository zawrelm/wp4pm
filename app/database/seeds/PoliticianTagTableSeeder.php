<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 22.08.14
 * Time: 10:25
 */

class PoliticianTagTableSeeder extends Seeder {

    public function run()
    {
        DB::table('politician_tags')->delete();

        PoliticianTag::create(array(
            'tag' => 'Fischer',
            'politician_id' => 1
        ));

        PoliticianTag::create(array(
            'tag' => 'Heinz Fischer',
            'politician_id' => 1
        ));

        PoliticianTag::create(array(
            'tag' => 'Faymann',
            'politician_id' => 2
        ));

        PoliticianTag::create(array(
            'tag' => 'Werner Faymann',
            'politician_id' => 2
        ));

        PoliticianTag::create(array(
            'tag' => 'Spindelegger',
            'politician_id' => 3
        ));

        PoliticianTag::create(array(
            'tag' => 'Michael Spindelegger',
            'politician_id' => 3
        ));

        PoliticianTag::create(array(
            'tag' => 'Strache',
            'politician_id' => 4
        ));

        PoliticianTag::create(array(
            'tag' => 'Heinz-Christian Strache',
            'politician_id' => 4
        ));

        PoliticianTag::create(array(
            'tag' => 'Glawischnig',
            'politician_id' => 5
        ));

        PoliticianTag::create(array(
            'tag' => 'Eva Glawischnig',
            'politician_id' => 5
        ));

        PoliticianTag::create(array(
            'tag' => 'Bucher',
            'politician_id' => 6
        ));

        PoliticianTag::create(array(
            'tag' => 'Josef Bucher',
            'politician_id' => 6
        ));

        PoliticianTag::create(array(
            'tag' => 'Frank Stronach',
            'politician_id' => 7
        ));

        PoliticianTag::create(array(
            'tag' => 'Mitterlehner',
            'politician_id' => 8
        ));

        PoliticianTag::create(array(
            'tag' => 'Josef Mitterlehner',
            'politician_id' => 8
        ));

        PoliticianTag::create(array(
            'tag' => 'Nachbaur',
            'politician_id' => 9
        ));

        PoliticianTag::create(array(
            'tag' => 'Kathrin Nachbaur',
            'politician_id' => 9
        ));

        PoliticianTag::create(array(
            'tag' => 'Häupl',
            'politician_id' => 10
        ));
        
        PoliticianTag::create(array(
            'tag' => 'Michael Häupl',
            'politician_id' => 10
        ));

        PoliticianTag::create(array(
            'tag' => 'Strolz',
            'politician_id' => 11
        ));

        PoliticianTag::create(array(
            'tag' => 'Matthias Strolz',
            'politician_id' => 11
        ));
		
        PoliticianTag::create(array(
            'tag' =>  'Heinisch-Hosek',
            'politician_id' => 12
        ));

        PoliticianTag::create(array(
            'tag' =>  'Schelling',
            'politician_id' => 13
        ));

        PoliticianTag::create(array(
            'tag' =>  'Erwin Pröll',
            'politician_id' => 14
        ));

        PoliticianTag::create(array(
            'tag' =>  'Niessl',
            'politician_id' => 15
        ));

        Politician::create(array(
            'tag' =>  'Brandstetter',
            'politician_id' => 16
        ));

        Politician::create(array(
            'tag' =>  'Gerald Klug',
            'politician_id' => 17
        ));
		

    }

}