<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 22.08.14
 * Time: 11:15
 */

class TypeOfPositionTagTableSeeder extends Seeder {

    public function run()
    {
        DB::table('type_of_position_tags')->delete();

        TypeOfPositionTag::create(array(
            'tag' => 'Bundesparteivorsitzende',
            'type_of_position_id' => 1
        ));

        TypeOfPositionTag::create(array(
            'tag' => '-Vorsitzende',
            'type_of_position_id' => 1
        ));

        TypeOfPositionTag::create(array(
            'tag' => 'Bundesparteiob',
            'type_of_position_id' => 1
        ));

        TypeOfPositionTag::create(array(
            'tag' => 'Bundespräsident',
            'type_of_position_id' => 2
        ));

/*        TypeOfPositionTag::create(array(
            'tag' => 'Präsident',
            'type_of_position_id' => 2
        ));*/

/*        TypeOfPositionTag::create(array(
            'tag' => 'Bundeskanzler',
            'type_of_position_id' => 3
        ));*/

        TypeOfPositionTag::create(array(
            'tag' => 'Kanzler',
            'type_of_position_id' => 3
        ));

        TypeOfPositionTag::create(array(
            'tag' => 'Vizekanzler',
            'type_of_position_id' => 4
        ));

/*        TypeOfPositionTag::create(array(
            'tag' => 'Bundesminister',
            'type_of_position_id' => 5
        ));*/

        TypeOfPositionTag::create(array(
            'tag' => 'Minister',
            'type_of_position_id' => 5
        ));

        TypeOfPositionTag::create(array(
            'tag' => 'Landeshaupt',
            'type_of_position_id' => 6
        ));

        TypeOfPositionTag::create(array(
            'tag' => 'LH',
            'type_of_position_id' => 6
        ));

        TypeOfPositionTag::create(array(
            'tag' => 'Bürgermeister',
            'type_of_position_id' => 7
        ));

        TypeOfPositionTag::create(array(
            'tag' => 'Nationalrat',
            'type_of_position_id' => 8
        ));

        TypeOfPositionTag::create(array(
            'tag' => 'Abgeordnete',
            'type_of_position_id' => 8
        ));

        TypeOfPositionTag::create(array(
            'tag' => 'Bundesrat',
            'type_of_position_id' => 9
        ));

        TypeOfPositionTag::create(array(
            'tag' => 'Präsident',
            'type_of_position_id' => 10
        ));

        TypeOfPositionTag::create(array(
            'tag' => 'Präsident',
            'type_of_position_id' => 11
        ));

        TypeOfPositionTag::create(array(
            'tag' => 'Leiter',
            'type_of_position_id' => 11
        ));

        TypeOfPositionTag::create(array(
            'tag' => 'Landtag',
            'type_of_position_id' => 12
        ));

        TypeOfPositionTag::create(array(
            'tag' => 'Gemeinderat',
            'type_of_position_id' => 13
        ));

        TypeOfPositionTag::create(array(
            'tag' => 'Landesparteivorsitzende',
            'type_of_position_id' => 14
        ));

        TypeOfPositionTag::create(array(
            'tag' => '-Vorsitzende',
            'type_of_position_id' => 14
        ));

        TypeOfPositionTag::create(array(
            'tag' => 'EU-Abgeordnete',
            'type_of_position_id' => 15
        ));

    }
    
}
