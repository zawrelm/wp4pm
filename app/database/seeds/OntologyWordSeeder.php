﻿<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 21.10.14
 * Time: 19:50
 */

class OntologyWordSeeder extends Seeder {

    public function run()
    {
        DB::table('ontology_words')->delete();

        OntologyWord::create(array(
            'word' => 'muss',
            'quotes_accepted' => 31,
            'quotes_rejected' => 15
        ));

        OntologyWord::create(array(
            'word' => 'wird',
            'quotes_accepted' => 22,
            'quotes_rejected' => 28
        ));

        OntologyWord::create(array(
            'word' => 'soll',
            'quotes_accepted' => 14,
            'quotes_rejected' => 34
        ));

        OntologyWord::create(array(
            'word' => 'darf',
            'quotes_accepted' => 19,
            'quotes_rejected' => 25
        ));

        OntologyWord::create(array(
            'word' => 'Herr',
            'quotes_accepted' => 17,
            'quotes_rejected' => 16
        ));

        OntologyWord::create(array(
            'word' => 'Frau',
            'quotes_accepted' => 13,
            'quotes_rejected' => 11
        ));

        OntologyWord::create(array(
            'word' => 'vermutlich',
            'quotes_accepted' => 4,
            'quotes_rejected' => 29
        ));

        OntologyWord::create(array(
            'word' => 'sollte',
            'quotes_accepted' => 11,
            'quotes_rejected' => 30
        ));

        OntologyWord::create(array(
            'word' => 'voraussichtlich',
            'quotes_accepted' => 4,
            'quotes_rejected' => 15
        ));

        OntologyWord::create(array(
            'word' => 'eher',
            'quotes_accepted' => 18,
            'quotes_rejected' => 40
        ));

        OntologyWord::create(array(
            'word' => 'wenig',
            'quotes_accepted' => 11,
            'quotes_rejected' => 31
        ));

        OntologyWord::create(array(
            'word' => 'relativ',
            'quotes_accepted' => 3,
            'quotes_rejected' => 17
        ));

        OntologyWord::create(array(
            'word' => 'Unwahrheit',
            'quotes_accepted' => 3,
            'quotes_rejected' => 1
        ));

        OntologyWord::create(array(
            'word' => 'inakzeptabel',
            'quotes_accepted' => 20,
            'quotes_rejected' => 11
        ));

        OntologyWord::create(array(
            'word' => 'bedenklich',
            'quotes_accepted' => 9,
            'quotes_rejected' => 19
        ));
        OntologyWord::create(array(
            'word' => 'zuständig',
            'quotes_accepted' => 5,
            'quotes_rejected' => 24
        ));

        OntologyWord::create(array(
            'word' => 'Minister',
            'quotes_accepted' => 24,
            'quotes_rejected' => 29
        ));

        OntologyWord::create(array(
            'word' => 'niemals',
            'quotes_accepted' => 20,
            'quotes_rejected' => 13
        ));

        OntologyWord::create(array(
            'word' => 'keinesfalls',
            'quotes_accepted' => 12,
            'quotes_rejected' => 3
        ));

        OntologyWord::create(array(
            'word' => 'keinen Fall',
            'quotes_accepted' => 16,
            'quotes_rejected' => 9
        ));

        OntologyWord::create(array(
            'word' => 'mit mir',
            'quotes_accepted' => 9,
            'quotes_rejected' => 6
        ));

        OntologyWord::create(array(
            'word' => 'klar',
            'quotes_accepted' => 25,
            'quotes_rejected' => 36
        ));

        OntologyWord::create(array(
            'word' => 'sage',
            'quotes_accepted' => 31,
            'quotes_rejected' => 90
        ));

        OntologyWord::create(array(
            'word' => 'deutlich',
            'quotes_accepted' => 15,
            'quotes_rejected' => 17
        ));
        
        OntologyWord::create(array(
            'word' => 'definitiv',
            'quotes_accepted' => 14,
            'quotes_rejected' => 10
        ));
        
        OntologyWord::create(array(
            'word' => 'Schande',
            'quotes_accepted' => 8,
            'quotes_rejected' => 7
        ));

        OntologyWord::create(array(
            'word' => 'interessant',
            'quotes_accepted' => 2,
            'quotes_rejected' => 9
        ));

        OntologyWord::create(array(
            'word' => 'Öko',
            'quotes_accepted' => 4,
            'quotes_rejected' => 6
        ));

    }

}
