<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 21.08.14
 * Time: 16:50
 */

class PartyTableSeeder extends Seeder {

    public function run()
    {
        DB::table('parties')->delete();

        Party::create(array(
            'name' => 'Sozialdemokratische Partei Österreichs',
            'shortname' => 'SPÖ',
            'link' => 'http://spoe.at'
        ));

        Party::create(array(
            'name' => 'Österreichische Volkspartei',
            'shortname' => 'ÖVP',
            'link' => 'http://oevp.at'
        ));

        Party::create(array(
            'name' => 'Freiheitliche Partei Österreichs',
            'shortname' => 'FPÖ',
            'link' => 'http://fpoe.at'
        ));

        Party::create(array(
            'name' => 'Die Grünen – Die Grüne Alternative',
            'shortname' => 'Grüne',
            'link' => 'http://www.gruene.at/'
        ));

        Party::create(array(
            'name' => 'Team Stronach für Österreich',
            'shortname' => 'Stronach',
            'link' => 'http://www.teamstronach.at/'
        ));

        Party::create(array(
            'name' => 'NEOS – Das Neue Österreich und Liberales Forum',
            'shortname' => 'NEOS',
            'link' => 'https://neos.eu'
        ));

        Party::create(array(
            'name' => 'Bündnis Zukunft Österreich - BZÖ',
            'shortname' => 'BZÖ',
            'link' => 'http://www.bzoe.at/'
        ));

        Party::create(array(
            'name' => 'Kommunistische Partei Österreichs',
            'shortname' => 'KPÖ',
            'link' => 'http://kpoe.at'
        ));

        Party::create(array(
            'name' => 'Piratenpartei Österreichs',
            'shortname' => 'Piraten',
            'link' => 'https://www.piratenpartei.at/'
        ));

        Party::create(array(
            'name' => 'Funktion der öffentlichen Verwaltung einer Gemeinde, eines Landes oder der Republik Österreich',
            'shortname' => 'Amtsträger',
            'link' => 'nicht verfügbar'
        ));

        Party::create(array(
            'name' => 'parteilos',
            'shortname' => 'parteilos',
            'link' => 'nicht verfügbar'
        ));

    }

}
