<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 21.08.14
 * Time: 20:05
 */

class FieldTagTableSeeder extends Seeder {

    public function run()
    {
        DB::table('field_tags')->delete();

    /* Level 1: Main Political Fields (1-15) */
    //TODO: THINK ABOUT WHETHER LEVEL 1 TAGS ARE REQUIRED (BESIDES FINANCE)
    //TODO: THINK ABOUT WHETHER IT MAKES SENSE TO GIVE THE TAGS A LEVEL OF SIGNIFICANCE (IF A HIGHER OCCURS, THIS IS TAKEN PRIMARILY)

        FieldTag::create(array(
            'tag' => 'Arbeitsrecht',
            'field_id' => 16
        ));

        FieldTag::create(array(
            'tag' => 'Gleichberechtigung',
            'field_id' => 16
        ));

        FieldTag::create(array(
            'tag' => 'equal',
            'field_id' => 16
        ));

        FieldTag::create(array(
            'tag' => 'Lohngleichheit',
            'field_id' => 16
        ));

        FieldTag::create(array(
            'tag' => 'Dienstleistung',
            'field_id' => 17
        ));

        FieldTag::create(array(
            'tag' => 'Industrie',
            'field_id' => 18
        ));

        FieldTag::create(array(
            'tag' => 'Konsumentenschutz',
            'field_id' => 19
        ));

        FieldTag::create(array(
            'tag' => 'Tourismus',
            'field_id' => 20
        ));

        FieldTag::create(array(
            'tag' => 'Europa',
            'field_id' => 21
        ));

        FieldTag::create(array(
            'tag' => 'international',
            'field_id' => 22
        ));

        FieldTag::create(array(
            'tag' => 'Außen',
            'field_id' => 22
        ));

        FieldTag::create(array(
            'tag' => 'Migration',
            'field_id' => 23
        ));

        FieldTag::create(array(
            'tag' => 'Integr',
            'field_id' => 23
        ));

        FieldTag::create(array(
            'tag' => 'Ehe ',
            'field_id' => 24
        ));

        FieldTag::create(array(
            'tag' => 'Partnerschaft',
            'field_id' => 24
        ));

        FieldTag::create(array(
            'tag' => 'Jugend',
            'field_id' => 25
        ));

        FieldTag::create(array(
            'tag' => 'Kindergeld',
            'field_id' => 25
        ));

        FieldTag::create(array(
            'tag' => 'Ärzte',
            'field_id' => 26
        ));

        FieldTag::create(array(
            'tag' => 'krankenkasse',
            'field_id' => 27
        ));

        FieldTag::create(array(
            'tag' => 'Spital',
            'field_id' => 28
        ));

        FieldTag::create(array(
            'tag' => 'Vorsorge',
            'field_id' => 29
        ));

        FieldTag::create(array(
            'tag' => 'Prävention',
            'field_id' => 29
        ));

        FieldTag::create(array(
            'tag' => 'Wohnbau',
            'field_id' => 30
        ));

        FieldTag::create(array(
            'tag' => 'wohnen',
            'field_id' => 30
        ));

        FieldTag::create(array(
            'tag' => 'energie',
            'field_id' => 31
        ));

        FieldTag::create(array(
            'tag' => 'IT-',
            'field_id' => 32
        ));

        FieldTag::create(array(
            'tag' => 'Datenschutz',
            'field_id' => 32
        ));

        FieldTag::create(array(
            'tag' => 'ÖPNV',
            'field_id' => 33
        ));

        FieldTag::create(array(
            'tag' => 'PKW',
            'field_id' => 33
        ));

        FieldTag::create(array(
            'tag' => 'LKW',
            'field_id' => 33
        ));

        FieldTag::create(array(
            'tag' => 'KFZ',
            'field_id' => 33
        ));

        FieldTag::create(array(
            'tag' => 'Maut',
            'field_id' => 33
        ));

        FieldTag::create(array(
            'tag' => 'Wasser',
            'field_id' => 34
        ));

        FieldTag::create(array(
            'tag' => 'Abfall',
            'field_id' => 34
        ));

        FieldTag::create(array(
            'tag' => 'Katastrophe',
            'field_id' => 35
        ));

        FieldTag::create(array(
            'tag' => 'Kriminalität',
            'field_id' => 36
        ));

        FieldTag::create(array(
            'tag' => 'Polizei',
            'field_id' => 36
        ));
        
        FieldTag::create(array(
            'tag' => 'Exekutiv',
            'field_id' => 36
        ));

        FieldTag::create(array(
            'tag' => 'Terrorismus',
            'field_id' => 36
        ));

        FieldTag::create(array(
            'tag' => 'wahlen',
            'field_id' => 37
        ));

        FieldTag::create(array(
            'tag' => 'Privatrecht',
            'field_id' => 38
        ));

        FieldTag::create(array(
            'tag' => 'Strafrecht',
            'field_id' => 39
        ));

        FieldTag::create(array(
            'tag' => 'Verwaltungsrecht',
            'field_id' => 40
        ));

        FieldTag::create(array(
            'tag' => 'vollzug',
            'field_id' => 41
        ));

        FieldTag::create(array(
            'tag' => 'Freizeit',
            'field_id' => 42
        ));

        FieldTag::create(array(
            'tag' => ' Kultur',
            'field_id' => 43
        ));

        FieldTag::create(array(
            'tag' => 'Medien',
            'field_id' => 44
        ));

        FieldTag::create(array(
            'tag' => 'ORF',
            'field_id' => 44
        ));

        FieldTag::create(array(
            'tag' => 'Forstwirtschaft',
            'field_id' => 45
        ));

        FieldTag::create(array(
            'tag' => 'Landwirtschaft',
            'field_id' => 46
        ));

        FieldTag::create(array(
            'tag' => 'Umwelt',
            'field_id' => 47
        ));

        FieldTag::create(array(
            'tag' => 'Bundesheer',
            'field_id' => 48
        ));

        FieldTag::create(array(
            'tag' => 'Militär',
            'field_id' => 48
        ));

        FieldTag::create(array(
            'tag' => 'Grenzschutz',
            'field_id' => 49
        ));

        FieldTag::create(array(
            'tag' => 'Behinderung',
            'field_id' => 50
        ));

        FieldTag::create(array(
            'tag' => 'Behinderten',
            'field_id' => 50
        ));

        FieldTag::create(array(
            'tag' => 'Pension',
            'field_id' => 51
        ));

        FieldTag::create(array(
            'tag' => 'Senioren',
            'field_id' => 51
        ));

        FieldTag::create(array(
            'tag' => 'Sozialversicherung',
            'field_id' => 52
        ));

        FieldTag::create(array(
            'tag' => 'Sport',
            'field_id' => 53
        ));

        FieldTag::create(array(
            'tag' => 'Budget',
            'field_id' => 54
        ));

        FieldTag::create(array(
            'tag' => 'steuer',
            'field_id' => 55
        ));

        FieldTag::create(array(
            'tag' => 'Finanz',
            'field_id' => 56
        ));

        FieldTag::create(array(
            'tag' => 'Fortbildung',
            'field_id' => 57
        ));

        FieldTag::create(array(
            'tag' => 'Erwachsenenbildung',
            'field_id' => 57
        ));
        
        FieldTag::create(array(
            'tag' => 'Volkshochschule',
            'field_id' => 57
        ));

        FieldTag::create(array(
            'tag' => 'Kindergart',
            'field_id' => 58
        ));

        FieldTag::create(array(
            'tag' => 'Schule',
            'field_id' => 59
        ));

        FieldTag::create(array(
            'tag' => 'Lehrer',
            'field_id' => 59
        ));

        FieldTag::create(array(
            'tag' => 'Forschung',
            'field_id' => 60
        ));

        FieldTag::create(array(
            'tag' => 'Universität',
            'field_id' => 61
        ));

    }
    
}
