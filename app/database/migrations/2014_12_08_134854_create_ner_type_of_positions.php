<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNerTypeOfPositions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ner_type_of_positions', function($table)
        {
            $table->increments('id');

            $table->integer('quote_id')->unsigned();
            $table->foreign('quote_id')->references('id')->on('quotes')->onUpdate('cascade')->onDelete('cascade');

            $table->integer('type_of_position_tag_id')->unsigned();
            $table->foreign('type_of_position_tag_id')->references('id')->on('type_of_position_tags')->onUpdate('cascade')->onDelete('cascade');            
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('ner_type_of_positions');
	}

}
