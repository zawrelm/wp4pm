<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePositions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('positions', function($table)
        {
            $table->increments('id');

            $table->integer('party_id')->unsigned();
            $table->foreign('party_id')->references('id')->on('parties');

            $table->integer('type_of_position_id')->unsigned();
            $table->foreign('type_of_position_id')->references('id')->on('type_of_positions');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('positions');
	}

}
