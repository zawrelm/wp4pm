<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('fields', function($table)
        {
            $table->increments('id');
            $table->string('name');

            $table->integer('parent_field_id')->unsigned()->nullable();
            $table->foreign('parent_field_id')->references('id')->on('fields')->onUpdate('cascade')->onDelete('cascade');            
        });
        
}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('fields');
	}

}
