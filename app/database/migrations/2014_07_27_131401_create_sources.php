<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSources extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('sources', function($table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('link');
            $table->integer('max_comments')->default(1); //highest number of reader comments to an article
			$table->integer('accepted_quotes')->default(0);//counts accepted quotes of this data sources
			$table->integer('rejected_quotes')->default(0);//counts rejected quotes of this data sources
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sources');
    }

}
