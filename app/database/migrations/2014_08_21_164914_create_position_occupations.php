<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePositionOccupations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('position_occupations', function($table)
        {
            $table->increments('id');
            $table->datetime('from');
            $table->datetime('to')->nullable();

            $table->integer('politician_id')->unsigned();
            $table->foreign('politician_id')->references('id')->on('politicians');

            $table->integer('position_id')->unsigned();
            $table->foreign('position_id')->references('id')->on('positions');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('position_occupations');
	}

}
