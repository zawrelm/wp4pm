<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliticianTags extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('politician_tags', function($table)
        {
            $table->increments('id');
            $table->string('tag');

            $table->integer('politician_id')->unsigned();
            $table->foreign('politician_id')->references('id')->on('politicians');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('politician_tags');
	}

}
