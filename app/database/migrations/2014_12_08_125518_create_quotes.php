<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('quotes', function($table)
        {
            $table->increments('id');
            $table->text('text');
            $table->float('semantic_score')->default(0.5);
            $table->float('community_score')->default(0.0);
            $table->integer('status')->default(0); //-1: deletable, 0: unprocessed, 1: automatically evaluated, 2: manually accepted, 3: manually rejected

            $table->integer('article_id')->unsigned();
            $table->foreign('article_id')->references('id')->on('articles');

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('quotes');
	}

}
