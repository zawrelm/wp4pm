<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldTags extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('field_tags', function($table)
        {
            $table->increments('id');
            $table->string('tag');

            $table->integer('field_id')->unsigned();
            $table->foreign('field_id')->references('id')->on('fields');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('field_tags');
	}

}
