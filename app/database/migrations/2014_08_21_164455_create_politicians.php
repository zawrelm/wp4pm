<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliticians extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('politicians', function($table)
        {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('title');
            //$table->datetime('date_of_birth');
            //$table->integer('gender');
            //$table->string('link_to_profile');
            //$table->string('twitter_hashtag');
            //$table->integer('twitter_retweet_threshold');
            //$table->integer('twitter_followers');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('politicians');
	}

}
