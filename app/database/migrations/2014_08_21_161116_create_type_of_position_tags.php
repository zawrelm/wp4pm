<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeOfPositionTags extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('type_of_position_tags', function($table)
        {
            $table->increments('id');
            $table->string('tag');

            $table->integer('type_of_position_id')->unsigned();
            $table->foreign('type_of_position_id')->references('id')->on('type_of_positions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('type_of_position_tags');
    }

}
