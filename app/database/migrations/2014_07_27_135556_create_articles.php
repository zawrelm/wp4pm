<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('articles', function($table)
        {
            $table->increments('id');
            $table->text('text');
            $table->string('title');
            $table->string('subheadline');
            $table->datetime('date');
            $table->string('author');
            $table->string('link')->unique();
            $table->integer('processed')->default(0);   //-1: deletable, 0: retrieved, 1: relevant, 2: quotes recognized, 3: NER performed, 4: processed (quotes existing)

            $table->integer('source_id')->unsigned()->nullable();
            $table->foreign('source_id')->references('id')->on('sources');

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('articles');
	}

}
