<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOntologyWords extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ontology_words', function($table)
        {
            $table->increments('id');
            $table->string('word')->unique();

            $table->integer('quotes_accepted')->unsigned()->default(0);
            $table->integer('quotes_rejected')->unsigned()->default(0);
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ontology_words');
	}

}
