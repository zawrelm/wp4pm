<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/

/* The following commands perform actions on Articles and Quotes and should be processed in the given order.
 *
 * Articles.processed = -1: deletable, 0: retrieved, 1: relevant, 2: quotes recognized, 3: NER performed, 4: processed (quotes existing)
 * Quotes.status = -1: deletable, 0: unprocessed, 1: automatically evaluated, 2: manually accepted, 3: manually rejected
 */

Artisan::add(new UpdateArticles); //produces articles with field "processed" at value 0 (retrieved)
Artisan::add(new CheckRelevanceOfArticles); //checks articles with "processed" = 0 and sets field "processed" to 1 (relevant) or -1 (deletable)
Artisan::add(new IdentifyQuotes); //scans relevant articles ("processed" = 1) for direct quotes, creates quote-entries with "status" = 0 (unprocessed) and sets article's "processed" = 2 (quotes recognized) or -2 (deletable)
Artisan::add(new EvaluateQuotes); //evaluates significance of quotes with "status" = 0, sets "status" = 1 (automatically processed) or -1 (deletable) and sets score-values
Artisan::add(new TagQuotes); //scans articles with relevant quotes for: political fields, politicians, parties and political positions. "processed" is set to 3 (NER performed)

//After automatic processing, quotes can be reviewed manually using the web-UI. quotes are set to "status" = 2 (manually accepted) or -2 (manually rejected - deletable).
//Moreover, if at least one quote of an article has been manually accepted, the article's "processed" field is set to 4 (processed, quotes existing).
//If all quotes of an article have been manually rejected, its "processed" field is set to -4 (deletable).
