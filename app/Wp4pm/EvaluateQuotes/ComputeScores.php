<?php
/**
 * User: Michael
 * Date: 13.12.14
 * Time: 13:03
 */

namespace Wp4pm\EvaluateQuotes;

class ComputeScores {

    function __construct(\EvaluateQuotes $evaluateQuotes)
    {
        $this->evaluateQuotes = $evaluateQuotes;
    }

    /**
     * Evaluates significance of quotes and sets score-values
     *
     * @return mixed
     */
    function evaluateQuotes()
    {
        try {
            
            $quotes = \Quote::where('status', '=', 0)->get();

            foreach($quotes as $quote)
            {
                $this->evaluateQuotes->message('info', "- Evaluating $quote->id...");
				$source = $quote->article->source;
				$quote->status = -1;
				
				$parts = explode('"', $quote->text);	//extract the quote's direct speech part - each second part is a direct quote
				$words = array();

                /* CALCULATE COMMUNITY SCORE */
                //retrieve quote, its article, postings and the article's source
                $mentions = 0;
                $upvotes = 0;
                $no_of_posts = count($quote->article->posts);
                
				for($i = 0; $i < count($parts); $i += 2)	//Iterate through all the direct quote parts...
				{
					if($i == 0 && count($parts) > 2) $i++;	//Special Case: necessary for quotes with quotation marks (direct speech in impair parts)
					$words = explode(' ', $parts[$i]);		//... collect all the words
					$this->evaluateQuotes->message('comment', "part: " . $parts[$i]);

					if(strlen($parts[$i]) > 5 && count($words) > 1)	//Ensure that quote has a reasonable size and has not been accepted by mistake in QuoteRecognition!
					{
						$quote->status = 1;
					}

                    //search for the quote within the postings and count hits
                    foreach($quote->article->posts as $post)
                    {
                        if(stripos($post->text, $parts[$i]) !== false)
                        {
                            $mentions++;
                            $upvotes += $post->up;
                        }                    
                    }
					if($i == 0 && count($parts) == 2) $i--;	//Special Case: necessary for quotes with one single quotation mark (direct speech part unknown)
				}

                //calculate community score
        		//community_score = log(no of article comments) / log(max comments of source) * W_FACTOR_1 + (no of mentions + upvotes) / sqrt(max comments) * W_FACTOR_2
				$community_score = log($no_of_posts) / log($source->max_comments) * 0.7 + ($mentions + $upvotes) / sqrt($source->max_comments) * 0.7;
				
                if ($community_score > 1.0) $community_score = 1.0;
                $this->evaluateQuotes->message('info', 'mentions = ' . $mentions . ', upvotes = ' . $upvotes 
                    . ', max_comments = ' . $source->max_comments . ', posts = ' . $no_of_posts);
                
                //write score to database
                $this->evaluateQuotes->message('info', 'community: ' . $community_score);
                $quote->community_score = $community_score;
                $quote->save();
                
                
                /* CALCULATE LINGUISTIC/SEMANTIC SCORE */
                $semantic_score = 0.0;
                
                // 1. basic points: check if person is in database and how important her position is (important position -> higher relevance)
                
                // 2. check length of quote (one-word quotes only in headlines, otherwise "deletable") -> already implemented in QuoteRecognition and community score

                // 3. use experience for computing score: SC = (f1(w1) + f1(w2) + ... + f1(wn))/n/f2(source)*2 => whereas for non-existing wx: f(wx) = f2(source)

                $n = count($words);
				$weighted_sum = 0.0;
				$source_acc_rate = 0.1;
				if($source->accepted_quotes > 1 && $source->rejected_quotes > 1)
					$source_acc_rate = $source->accepted_quotes / ($source->accepted_quotes + $source->rejected_quotes);
                
				for($i = 0; $i < $n; $i++)
                {
					$word = \OntologyWord::where('word', $words[$i])->first();
					$f_word = 0.0;
					if($word == null) $f_word = $source_acc_rate;
					else $f_word = $word->quotes_accepted/($word->quotes_accepted+$word->quotes_rejected);
                    $weighted_sum += $f_word;
                }
				
				$semantic_score += $weighted_sum/$n/($source_acc_rate*2);	//values averaged around 0.5 by considering rate of accepted quotes for the source
                
                // 4. check position of quote (if its the headline -> more valuable)
                if(count($parts) > 2 && strpos($parts[1], $quote->article->title)) $semantic_score += 0.2;
                
                //write score to database
                $this->evaluateQuotes->message('info', 'n = ' . $n . ', weighted_sum = ' . $weighted_sum
					. ', source_acc_rate = ' . $source_acc_rate);
                if ($semantic_score > 1.0) $semantic_score = 1.0;
                if ($semantic_score < 0.0) $semantic_score = 0.0;
                $this->evaluateQuotes->message('info', 'semantic: ' . $semantic_score);
                $quote->semantic_score = $semantic_score;
				$quote->save();
                
            }
        }
        catch(QueryException $e) {
            $this->evaluateQuotes->message('comment', 'QueryException on loading Quotes!');
        }
    }

}
 