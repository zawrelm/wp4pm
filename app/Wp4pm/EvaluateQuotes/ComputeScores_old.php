<?php
/**
 * User: Michael
 * Date: 13.12.14
 * Time: 13:03
 */

namespace Wp4pm\EvaluateQuotes;

class ComputeScores {

    function __construct(\EvaluateQuotes $evaluateQuotes)
    {
        $this->evaluateQuotes = $evaluateQuotes;
    }

    /**
     * Evaluates significance of quotes and sets score-values
     *
     * @return mixed
     */
    function evaluateQuotes()
    {
        try {
            
            $ontology = \OntologyWord::all();
            $quotes = \Quote::where('status', '=', 0)->get();

            foreach($quotes as $quote)
            {
                $this->evaluateQuotes->message('info', "- Evaluating $quote->id...");

                /* CALCULATE COMMUNITY SCORE */
                //retrieve quote, its article, postings and the article's source
                $mentionings = 0;
                $upvotes = 0;
                $community_size = \Source::find($quote->article->source_id)->community_size;
                $no_of_posts = count($quote->article->posts);
                
                //extract the quotes direct speech part
                if(preg_match('/"(.*?)"/', $quote->text, $direct_speech))
                {
                    foreach($direct_speech as $pattern)
                    {
                        $this->evaluateQuotes->message('comment', "part: " . $pattern);

                        //search for the quote within the postings and count hits
                        foreach($quote->article->posts as $post)
                        {
                            if(stripos($post->text, $pattern) !== false)
                            {
                                $mentionings++;
                                $upvotes += $post->up;
                            }                    
                        }
                    
                    }
                }
        
                //calculate community score
        		//community_score = (comments mentioning quote + 0.04*sum of upvotes) / community size factor + number of article comments / (100*community size factor)
                $community_score = (($mentionings + 0.04*$upvotes) / $community_size) + ($no_of_posts / (100*$community_size));
                if ($community_score > 1.0) $community_score = 1.0;
                $this->evaluateQuotes->message('info', 'mentionings = ' . $mentionings . ', upvotes = ' . $upvotes 
                    . ', com_size = ' . $community_size . ', posts = ' . $no_of_posts);
                
                //write score to database
                $this->evaluateQuotes->message('info', 'community: ' . $community_score);
                $quote->community_score = $community_score;
                $quote->save();
                
                
                /* CALCULATE LINGUISTIC/SEMANTIC SCORE */
                $semantic_score = 0.5;
                
                // 1. basic points: check if person is in database and how important her position is (important position -> higher relevance)
                
                // 2. check length of quote (one word quotes only in headlines, otherwise "deletable") -> already implemented in QuoteRecognition

                // 3. check position of quote (if its the headline -> more valuable)
                if(strpos($quote->text, $quote->article->title)) $semantic_score += 0.2;
                
                // 4. use ontology for increasing/decreasing score
                $quotelength = count(explode(' ', $quote->text));
                foreach($ontology as $word)
                {
                    if(strpos($word->word, $quote->text) !== false)
                    {
                        $this->evaluateQuotes->message('info', 'hit: ' . $word->word);
                        
                        //contribution = ((accepted quotes - rejected quotes) / occurances) / quotelength
                        $semantic_score += (($word->quotes_accepted - $word->quotes_rejected) / (2*($word->quotes_accepted + $word->quotes_rejected))) /$quotelength;
                        
                    }
                }
                
                //write score to database
                if ($semantic_score > 1.0) $semantic_score = 1.0;
                if ($semantic_score < 0.0) $semantic_score = 0.0;
                $this->evaluateQuotes->message('info', 'semantic: ' . $semantic_score);
                $quote->semantic_score = $semantic_score;
                //TODO: $quote->status = 1;
                $quote->save();
                
            }
        }
        catch(QueryException $e) {
            $this->checkArticleRelevance->message('comment', 'QueryException on loading Articles!');
        }
    }

}
 