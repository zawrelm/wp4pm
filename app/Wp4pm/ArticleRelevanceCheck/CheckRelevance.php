<?php
/**
 * User: Michael
 * Date: 09.12.14
 * Time: 17:45
 */

namespace Wp4pm\ArticleRelevanceCheck;

class CheckRelevance {

    function __construct(\CheckRelevanceOfArticles $checkArticleRelevance)
    {
        $this->checkArticleRelevance = $checkArticleRelevance;
    }

    /**
     * Loads unprocessed articles and checks their relevance for the program
     *
     * @return mixed
     */
    function checkArticles()
    {
        try {
            $articles = \Article::where('processed', '=', 0)->get();

            foreach($articles as $article)
            {
                $this->checkArticleRelevance->message('info', "- Checking $article->title...");
                $relevant = -1;

                /* EXTRACT TEXT */
                $puretext = strip_tags($article->title . '. ' . $article->subheadline . '. ' . $article->text);

                /* SEARCH FOR POLITICAL PARTIES */
                foreach(\PartyTag::all() as $partyTag)
                {
                    //$this->checkArticleRelevance->message('info', "Search for $partyTag->tag");
                    if(stripos($puretext, $partyTag->tag) !== false)
                    {
                        $relevant = 1;
                        break;
                    }
                }
                
                /* SEARCH FOR POLITICIANS *
                if($relevant == -1)
                {
                    foreach(\PoliticianTag::all() as $politicianTag)
                    {
                        //$this->checkArticleRelevance->message('info', "Search for $politicianTag->tag");
                        if(strpos($puretext, $politicianTag->tag) !== false)
                        {
                            $relevant = 1;
                            break;
                        }
                    }
                }
                
                /* SEARCH FOR NEUTRAL POSITIONS *
                if($relevant == -1)
                {
                    foreach(\TypeOfPositionTag::all() as $typeOfPositionTag)
                    {
                        //$this->checkArticleRelevance->message('info', "Search for $typeOfPositionTag->tag");
                        if(strpos($puretext, $typeOfPositionTag->tag) !== false)
                        {
                            $relevant = 1;
                            break;
                        }
                    }
                }*/
                
                //set article's processed field to 1 = 'relevant' or -1 = 'deletable'
                $this->checkArticleRelevance->message('info', "relevant = $relevant");
                $article->processed = $relevant;
                $article->save();

            }
        }
        catch(QueryException $e) {
            $this->checkArticleRelevance->message('comment', 'QueryException on loading Articles!');
        }
    }

}
 