<?php
/**
 * User: Michael
 * Date: 05.12.14
 * Time: 10:14
 */

namespace Wp4pm\QuoteRecognition;

class QuoteRecognition {

    function __construct(\IdentifyQuotes $identifyQuotes)
    {
        $this->identifyQuotes = $identifyQuotes;
    }

    /**
     * Loads relevant articles and searches for quotes
     *
     * @return mixed
     */
    function scanArticles()
    {
        try {
            $articles = \Article::where('processed', '=', 1)->get();

            foreach($articles as $article)
            {
                $this->identifyQuotes->message('info', "- Scanning $article->title...");
                $relevant = -2;

                /* TOKENIZE */
                $paragraphs = explode('<p>', $article->title . '<p>' . $article->subheadline . '<p>' . $article->text);

                foreach($paragraphs as $paragraph)
                {
                    //remove tags
                    $puretext = strip_tags($paragraph);

                    //split for sentences and keep the delimiters
                    $parts = preg_split('/([^(0-9)][\.\?\!]\"?\s)/', $puretext, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY); //|bzw.|etc.|usw.|z.B.|Chr.|vgl.
                    $sentences = array();
                    for($i = 0, $count = count($parts); $i < $count-1; $i += 2)
                    {
                        $sentences[] = $parts[$i] . $parts[$i + 1];
                    }
                    if(count($parts) % 2) $sentences[] = $parts[count($parts)-1];
                    //$this->identifyQuotes->message('error', 'parts: ' . count($parts) . ', sentences: ' . count($sentences));

                    /* IDENTIFY QUOTES */
                    //search for beginnings and endings of quotes
                    $isWithinQuote = false;
                    $doubleQuoteCounter = 0;
                
                    foreach($sentences as $sentence)
                    {
                        $doubleQuoteCounter += substr_count($sentence, '"');
                        //$this->identifyQuotes->message('info', $doubleQuoteCounter);
                        //$this->identifyQuotes->message('info', $sentence);
                    
                        //if the sentence is not within an already opened quotation and doesn't contain new quotation marks, it is not a quote
                        if($doubleQuoteCounter == 0) continue;

                        //if there are only two quotation marks parenthesizing a single word, it is not a relevant quote
                        if($doubleQuoteCounter == 2 && preg_match('/"[^\s-]+"/', $sentence) == 1)
                        {
                            $doubleQuoteCounter = 0;
                            //$this->identifyQuotes->message('comment', 'eliminated: ' . $sentence);
                            continue;
                        }

                        //if the number of quotation marks is even (and >0), a quote is over
                        if($doubleQuoteCounter%2 == 0) $doubleQuoteCounter = 0;

                        //option: if $doubleQuoteCounter is an odd number, the same quote continues and there is no new one created

                        //write sentence as it is to database
                        $this->createQuote($sentence, $article->id);
                        $relevant = 2;
                    }                    
                }
                
                //set articles processed field to 2 = 'quotes recognized' or -2 (deletable)
                $article->processed = $relevant;
                $article->save();

            }
        }
        catch(QueryException $e) {
            $this->identifyQuotes->message('comment', 'QueryException on loading Articles!');
        }
    }

	private function createQuote($text, $article_id)
	{
		try {
            $quote = \Quote::create([
                'text' => $text,
                'article_id' => $article_id
            ]);
        } catch(QueryException $e) {
            $this->identifyQuotes->message('comment', 'QueryException on Quote of article ' . $article_id);
        }
	}
    
}
 