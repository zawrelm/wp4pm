<?php
/**
 * User: Michael
 * Date: 30.07.14
 * Time: 16:12
 */

namespace Wp4pm\Crawler;

use Carbon\Carbon;
use Goutte\Client;
use Illuminate\Database\QueryException;
use Symfony\Component\DomCrawler\Crawler;

class DiePresseArticleSource extends AbstractRSSArticleSource {

    private static $sourceName = 'diepresse.com';

    private static $rootURLs = [
        'http://diepresse.com/rss/Politik',
        'http://diepresse.com/rss/EU'
    ];

    function parseArticle($url)
    {
        // http client (crawler, browser)
        $client = new Client();

        //try loading the print version of the article for easier text handling
        if(($crawler = $this->tryLoadingSiteTwice($client, dirname($url) . '/print.do')) === NULL) return null;
        $this->updateArticles->message('comment', 'print version loaded');

        $subheadline = $crawler->filter('p.articlelead')->first()->html();
        $body = $crawler->filter('div.articletext')->first()->html();

        try {
            $article = \Article::create([
                'link' => $url,
                'subheadline' => $subheadline,
                'text' => $body,
                'source_id' => static::getSourceId()
            ]);
        } catch(QueryException $e) {
            $this->updateArticles->message('comment', 'QueryException on Article with url ' . $url);
            return NULL;
        }

        //try loading the article's full version for comment handling
        if(($crawler = $this->tryLoadingSiteTwice($client, $url)) === NULL) return null;
        $this->updateArticles->message('comment', 'full version loaded');

        // for each comment site
        do {

            $this->updateArticles->message('info', '   +++ next page +++');

            //if(isset($linkElement)) $this->updateArticles->message('info', $linkElement->link()->getUri());

            // iterate through comments
            $crawler->filter('div.commentWrapper')->each(function(Crawler $node, $i) use ($article)
            {
                // data
                $uid = $node->filter('a')->attr('id');
                $poster = $node->filter('.username')->text();
                $date = new Carbon(); //var_dump(preg_match_all('/\d\d\.\d\d\.\d\d\d\d \d\d:\d\d/', $node->filter('div.commentfrom')->text())));
                $up = $node->filter('span.stand')->text();
                $comment = $node->filter('.commentText')->html();

                // create post!
                try {
                    \Post::create([
                        'author' => $poster,
                        'text' => $comment,
                        'date' => $date,
                        'up' => $up,
                        'down' => 0,
                        'article_id' => $article->id,
                        'uid' => $uid
                    ]);
                } catch(QueryException $e) {
                    $this->updateArticles->message('comment', 'QueryException on Post with uid ' . $uid);
                }
            });

            // search for navigation item to the next page of comments and set $linkElement to it, if existent
            $links = $crawler->filter('a.r');
            if($links->count() > 0) $linkElement = $links->first();

            // if navigation to next comments page exists
            if(isset($linkElement))
            {
                // !!!!
                // try setting the crawler (= current page) to the navigation elements destination
                if(($crawler = $this->tryLoadingSiteTwice($client, $linkElement->link()->getUri())) === NULL) return null;

            }
        } while(isset($linkElement) && $links->count() > 0);

        return $article;
    }

    /**
     * @return array
     */
    public static function getRootURLs()
    {
        return static::$rootURLs;
    }

    public static function getSourceName()
    {
        return static::$sourceName;
    }

    public static function getSourceId()
    {
        return \Source::where('name', '=', static::$sourceName)->get()->first()->id;
    }

}
