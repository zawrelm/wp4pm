<?php
/**
 * User: Michael
 * Date: 30.07.14
 * Time: 16:12
 */

namespace Wp4pm\Crawler;


use Awjudd\FeedReader\Facades\FeedReader;
use Carbon\Carbon;
use Goutte\Client;
use Guzzle\Http\Exception\CurlException;
use Illuminate\Database\QueryException;
use Symfony\Component\DomCrawler\Crawler;

abstract class AbstractRSSArticleSource extends AbstractArticleSource {

    function parseSite()
    {
        self::parseSiteRSS();
    }

    function parseSiteRSS()
    {
        $this->updateArticles->message('info', 'Parsing ' . static::getSourceName() . '...');

        // iterate through feeds
        foreach(static::getRootURLs() as $url)
        {
            $this->updateArticles->message('info', "- Parsing $url...");

            // get rss feed (simplepie - http://simplepie.org/api/class-SimplePie_Item.html)
            $rss = FeedReader::read($url);

            // for each item in the rss feed
            foreach($rss->get_items() as $key => $item)
            {
                $this->parseFeedItem($item);
            }
        }
    }

    /**
     * Extracts some data from RSS-feed's item and calls parseArticle with the URI of the article.
     *
     * @param $item single SimplePie_Item of the RSS-feed
     */
    function parseFeedItem($item)
    {
        // get data
        $link = $item->get_link(); // get link to the full article
        $title = $item->get_title();
        $date = new Carbon($item->get_date()); // carbon is a library for datetime, new Carbon('str') creates an object out of a string (internally uses strtotime)
        $date->setTimezone("Europe/Vienna");

        $this->updateArticles->message('info', "  - Article ($link) at $date: $title");

        // get article
        try {
            $article = $this->parseArticle($link);
        } catch(\Exception $e) {
            $this->updateArticles->message('error', 'Unknown Exception \'' . $e->getMessage() . '\', skipping article at ' . $link);
            return;
        }

        if($article === NULL)
        {
            $this->updateArticles->message('error', 'Could not parse article at ' . $link);
            return;
        }

        // fill article with remaining metadata
        $article->fill([
            'date' => $date,
            'title' => $title
        ]);

        $article->save();
		
		// set amount of posts if necessary
		$no_of_posts = $article->posts->count();
		$source = $article->source;
		if($no_of_posts > $source->max_comments)
		{
			$source->max_comments = $no_of_posts;
			$source->save();
		}
    }

}