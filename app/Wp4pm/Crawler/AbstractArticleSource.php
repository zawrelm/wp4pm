<?php
/**
 * User: Michael
 * Date: 27.07.14
 * Time: 17:59
 */

namespace Wp4pm\Crawler;

abstract class AbstractArticleSource {

    function __construct(\UpdateArticles $updateArticles)
    {
        $this->updateArticles = $updateArticles;
    }

    /**
     * Loads specific article and performs parsing of the contents into the database
     *
     * @param $url
     * @return mixed
     */
    abstract function parseArticle($url);

    /**
     * Loads web-datasource and searches for articles.
     *
     * @return mixed
     */
    abstract function parseSite();

    static function getRootURLs() { throw new RuntimeException("Unimplemented function getRootURLs"); }
    static function getSourceName() { throw new RuntimeException("Unimplemented function getSourceName"); }
    static function getSourceId() { throw new RuntimeException("Unimplemented function getSourceId"); }

    function tryLoadingSiteTwice($client, $url)
    {
        $tryagain = true;
        do {
            try {
                // HTTP get auf url
                return $client->request('GET', $url); // kann CurlException werfen
                break;
            }
            catch(CurlException $e)
            {
                if($tryagain)
                {
                    $tryagain = false;
                    $this->updateArticles->message('error', 'CurlException on ' . $url);
                }
                else
                {
                    return null;
                }

            }
        } while(true);
    }

} 