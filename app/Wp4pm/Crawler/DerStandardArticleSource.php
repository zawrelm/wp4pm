<?php
/**
 * User: Michael
 * Date: 27.07.14
 * Time: 18:01
 */

namespace Wp4pm\Crawler;

use Carbon\Carbon;
use Goutte\Client;
use Illuminate\Database\QueryException;
use Symfony\Component\DomCrawler\Crawler;

class DerStandardArticleSource extends AbstractRSSArticleSource {

    private static $sourceName = 'derstandard.at';

    private static $rootURLs = [
        'http://derstandard.at/?page=rss&ressort=inland'
    ];

    function parseArticle($url)
    {
        // http client (crawler, browser)
        // https://github.com/fabpot/Goutte
        $client = new Client();

        // crawler: http://symfony.com/doc/current/components/dom_crawler.html
        if(($crawler = $this->tryLoadingSiteTwice($client, $url)) === NULL) return null;

        // filters are css selectors on the content of the current page
        try {
			$subheadline = $crawler->filter('h2')->first()->html();
		} catch(QueryException $e) { $subheadline = ""; }
        $body = $crawler->filter('div.copytext')->first()->html();

        try {
            $article = \Article::create([
                'link' => $url,
                'subheadline' => $subheadline,
                'text' => $body,
                'source_id' => static::getSourceId()
            ]);
        } catch(QueryException $e) {
            $this->updateArticles->message('comment', 'QueryException on Article with url ' . $url);
            return NULL;
        }

        // for each comment site
		//$go_on = false;
        //do {

            $this->updateArticles->message('info', '   +++ next page +++');

            // iterate through comments
            $crawler->filter('div.posting-container')->each(function(Crawler $node, $i) use ($article)
            {
                // $node is a crawler object that only contains the partial tree of the current comment
				$this->updateArticles->message('comment', 'NEXT POSTING!');

                // data
                $uid = $node->attr('data-postingid');
                $poster = $node->attr('data-communityname');
				$date = new Carbon($node->filter('span.timestamp')->text(), 'Europe/Vienna');
				
                // upvotes and downvotes
                $up = $node->filter('span.p');
                $down = $node->filter('span.n');

                $comment = $node->filter('.posting-content')->html();

                // create post!
                try {
                    \Post::create([
                        'author' => $poster,
                        'text' => $comment,
                        'date' => $date,
                        'up' => $up,
                        'down' => $down,
                        'article_id' => $article->id,
                        'uid' => $uid
                    ]);
                } catch(QueryException $e) {
                    $this->updateArticles->message('comment', 'QueryException on Post with uid ' . $uid);
                }
            });

            // if navigation to next comments page exists
			//$button = $crawler->filter('button.next')->first();
            //$button = $crawler->selectButton('>');
            
        //} while(!str_contains($button->attr('disabled'), 'disabled'));

        return $article;
    }

    /**
     * @return array
     */
    public static function getRootURLs()
    {
        return static::$rootURLs;
    }

    public static function getSourceName()
    {
        return static::$sourceName;
    }

    public static function getSourceId()
    {
        return \Source::where('name', '=', static::$sourceName)->get()->first()->id;
    }

} 