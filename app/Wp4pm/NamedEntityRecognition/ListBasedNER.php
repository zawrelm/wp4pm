<?php
/**
 * User: Michael
 * Date: 11.12.14
 * Time: 13:24
 */

namespace Wp4pm\NamedEntityRecognition;


class ListBasedNER extends AbstractNER {

    function recognizeNamedEntities()
    {
        $this->tagQuotes->message('info', 'list-based named entity recognition started');
        
        try {
            $articles = \Article::where('processed', '=', 2)->get();

            foreach($articles as $article)
            {
                
                $quotes = $article->quotes;
                $this->tagQuotes->message('info', "- Checking $article->title... (" . count($quotes) . " Quotes)");
                
                /* TODO: maybe, in a first step, search for tags within parts of quotes that are not in quotation marks (foreach $article->quotes...)
                 * -> only search for named entities of those kinds, where no tags could be identified
                 * -> if there are multiple parties and positions identified, stick with the one of the person that is most likely the speaker */

                /* SEARCH WITHIN THE QUOTE */
                
                
                /* SEARCH WITHIN FULL ARTICLE */
                $puretext = strip_tags($article->title . '. ' . $article->subheadline . '. ' . $article->text);

                // SEARCH FOR POLITICAL FIELDS
                $identified_fields = array();
                foreach(\FieldTag::all() as $fieldTag)
                {
                    if(!in_array($fieldTag->field, $identified_fields) && stripos($puretext, $fieldTag->tag) !== false)
                    {
                        $identified_fields[] = $fieldTag->field;
                        //$this->tagQuotes->message('info', "Field: $fieldTag->field");

                        foreach($quotes as $quote) {
                            try {
                                $nerField = \NerField::create([
                                    'field_tag_id' => $fieldTag->id,
                                    'quote_id' => $quote->id
                                ]);
                            } catch(QueryException $e) {
                                $this->tagQuotes->message('comment', 'QueryException on setting FieldTag for Quote with id ' . $quote->id);
                            }

                        }
                    }
                }

                
                // SEARCH FOR POLITICIANS
                $identified_politicians = array();
                foreach(\PoliticianTag::all() as $politicianTag)
                {
                    if(!in_array($politicianTag->politician, $identified_politicians) && stripos($puretext, $politicianTag->tag) !== false)
                    {
                        $identified_politicians[] = $politicianTag->politician;
                        //$this->tagQuotes->message('info', "Politician: $politicianTag->politician");

                        foreach($quotes as $quote) {
                            try {
                                $nerPolitician = \NerPolitician::create([
                                    'politician_tag_id' => $politicianTag->id,
                                    'quote_id' => $quote->id
                                ]);
                            } catch(QueryException $e) {
                                $this->tagQuotes->message('comment', 'QueryException on setting PoliticianTag for Quote with id ' . $quote->id);
                            }

                        }
                    }
                }

    
                // SEARCH FOR POLITICAL PARTIES
                $identified_parties = array();
                foreach(\PartyTag::all() as $partyTag)
                {
                    if(!in_array($partyTag->party, $identified_parties) && stripos($puretext, $partyTag->tag) !== false)
                    {
                        $identified_parties[] = $partyTag->party;
                        //$this->tagQuotes->message('info', "Party: $partyTag->party");

                        foreach($quotes as $quote) {
                            try {
                                $nerParty = \NerParty::create([
                                    'party_tag_id' => $partyTag->id,
                                    'quote_id' => $quote->id
                                ]);
                            } catch(QueryException $e) {
                                $this->tagQuotes->message('comment', 'QueryException on setting PartyTag for Quote with id ' . $quote->id);
                            }

                        }
                    }
                }
                    
                // SEARCH FOR POLITICAL POSITIONS
                $identified_tops = array();
                foreach(\TypeOfPositionTag::all() as $typeOfPositionTag)
                {
                    if(!in_array($typeOfPositionTag->typeOfPosition, $identified_tops) && stripos($puretext, $typeOfPositionTag->tag) !== false)
                    {
                        $identified_tops[] = $typeOfPositionTag->typeOfPosition;
                        //$this->tagQuotes->message('info', "TypeOfPosition: $typeOfPositionTag->typeOfPosition");

                        foreach($quotes as $quote) {
                            try {
                                $nerTypeOfPosition = \NerTypeOfPosition::create([
                                    'type_of_position_tag_id' => $typeOfPositionTag->id,
                                    'quote_id' => $quote->id
                                ]);
                            } catch(QueryException $e) {
                                $this->tagQuotes->message('comment', 'QueryException on setting TypeOfPositionTag for Quote with id ' . $quote->id);
                            }

                        }
                    }
                }
                
                //set article's processed field to 3 = 'NER performed' in order to not execute the command twice onto the same article
                $article->processed = 3;
                $article->save();
                
            }
        }
        catch(QueryException $e) {
            $this->tagQuotes->message('comment', 'QueryException on loading Articles!');
        }

    }

}
