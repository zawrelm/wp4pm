<?php
/**
 * User: Michael
 * Date: 20.08.14
 * Time: 19:16
 */

namespace Wp4pm\NamedEntityRecognition;

abstract class AbstractNER {

    function __construct(\TagQuotes $tagQuotes)
    {
        $this->tagQuotes = $tagQuotes;
    }

    /**
     * Analyzes relevant articles with identified quotes for comprised people, parties, positions and topics
     *
     * @return mixed
     */
    abstract function recognizeNamedEntities();

} 