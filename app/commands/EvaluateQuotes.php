<?php

use Illuminate\Console\Command;
use Wp4pm\EvaluateQuotes\ComputeScores;

class EvaluateQuotes extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'quotes:evaluate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Analyzes the relevance of unprocessed quotes considering speaker, linguistical aspects and community reaction.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()     //TODO: parameter for selecting which score should be created (1 - semantic only, 2 - community only, rest - both) OR OPTION!
	{
        $computeScores = new ComputeScores($this);
        $computeScores->evaluateQuotes();
	}

    public function message($type, $text)
    {
        $this->{$type}($text);
    }

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
