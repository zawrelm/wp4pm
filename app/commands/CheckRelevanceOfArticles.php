<?php

use Illuminate\Console\Command;
use Wp4pm\ArticleRelevanceCheck\CheckRelevance;

class CheckRelevanceOfArticles extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'articles:check';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Checks yet unprocessed articles for their relevance within the Austrian political context.';

	/**
	 * Create a new command instance.
	 *
     * @return \CheckRelevanceOfArticles
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $checkRelevance = new CheckRelevance($this);
        $checkRelevance->checkArticles();
	}

    public function message($type, $text)
    {
        $this->{$type}($text);
    }

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
