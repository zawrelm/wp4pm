<?php

use Illuminate\Console\Command;
use Wp4pm\QuoteRecognition\QuoteRecognition;

class IdentifyQuotes extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'quotes:identify';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Tries to identify quotes out of relevant articles.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$quoteRecognition = new QuoteRecognition($this);
        $quoteRecognition->scanArticles();
	}

    public function message($type, $text)
    {
        $this->{$type}($text);
    }

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
