<?php

use Illuminate\Console\Command;
use Wp4pm\Crawler\DerStandardArticleSource;
use Wp4pm\Crawler\DiePresseArticleSource;

class UpdateArticles extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'articles:update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Starts crawler that searches for new articles in implemented data sources.';

    /**
     * Create a new command instance.
     *
     * @return \UpdateArticles
     */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $derStandard = new DerStandardArticleSource($this);
        $derStandard->parseSite();
        $diePresse = new DiePresseArticleSource($this);
        $diePresse->parseSite();
	}

    public function message($type, $text)
    {
        $this->{$type}($text);
    }

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
