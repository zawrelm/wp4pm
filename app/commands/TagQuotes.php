<?php

use Illuminate\Console\Command;
use Wp4pm\NamedEntityRecognition\ListBasedNER;

class TagQuotes extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'quotes:tag';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tries to recognize named entities in relevant articles that already have quotes identified.';

    /**
     * Create a new command instance.
     *
     * @return \TagArticles
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $lbNER = new ListBasedNER($this);
        $lbNER->recognizeNamedEntities();
    }

    public function message($type, $text)
    {
        $this->{$type}($text);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            //array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
