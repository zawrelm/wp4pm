<?php

class OntologyWord extends Eloquent {

    protected $fillable = ['word', 'quotes_accepted', 'quotes_rejected'];

    public $timestamps = false;

}
