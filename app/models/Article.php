<?php

class Article extends Eloquent {
        
    protected $fillable = ['text', 'title', 'subheadline', 'link', 'author', 'date', 'processed', 'source_id'];

    public function posts()
    {
        return $this->hasMany('Post');
    }

    public function quotes()
    {
        return $this->hasMany('Quote');
    }

    public function source()
    {
        return $this->belongsTo('Source');
    }

}
