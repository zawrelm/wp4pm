<?php

class Politician extends Eloquent {

    protected $fillable = ['firstname', 'lastname', 'title']; //'link_to_profile'

    public $timestamps = false;

    public function tags()
    {
        return $this->hasMany('PoliticianTag');
    }

} 