<?php

class Field extends Eloquent {

    protected $fillable = ['name', 'parent_field_id'];

    public $timestamps = false;

    public function tags()
    {
        return $this->hasMany('FieldTag');
    }

} 