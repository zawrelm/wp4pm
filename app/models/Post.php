<?php

/*use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;*/

class Post extends Eloquent {

    protected $fillable = ['text', 'up', 'down', 'author', 'date', 'article_id', 'uid'];

    public function article()
    {
        return $this->belongsTo('Article');
    }

}
