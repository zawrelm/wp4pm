<?php

class TypeOfPositionTag extends Eloquent {
        
    protected $fillable = ['tag', 'type_of_position_id'];

    public $timestamps = false;

    public function typeOfPosition()
    {
        return $this->belongsTo('TypeOfPosition');
    }

    public function quotes()
    {
        return $this->belongsToMany('Quote', 'ner_type_of_positions');
    }

}
