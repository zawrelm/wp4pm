<?php

class NerField extends Eloquent {
        
    protected $fillable = ['quote_id', 'field_tag_id'];

    public $timestamps = false;

    public function quote()
    {
        return $this->belongsTo('Quote');
    }

    public function fieldTag()
    {
        return $this->belongsTo('FieldTag');
    }

}
