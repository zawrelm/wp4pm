<?php

class PartyTag extends Eloquent {
        
    protected $fillable = ['tag', 'party_id'];

    public $timestamps = false;

    public function party()
    {
        return $this->belongsTo('Party');
    }

    public function quotes()
    {
        return $this->belongsToMany('Quote', 'ner_parties');
    }

}
