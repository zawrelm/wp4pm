<?php

class Source extends Eloquent {

    protected $fillable = ['name', 'link', 'max_comments', 'accepted_quotes', 'rejected_quotes'];

    public $timestamps = false;

    public function article()
    {
        return $this->belongsTo('Article');
    }

}
