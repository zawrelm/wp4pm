<?php

class Party extends Eloquent {

    protected $fillable = ['name', 'shortname', 'link'];

    public $timestamps = false;

    public function tags()
    {
        return $this->hasMany('PartyTag');
    }

} 