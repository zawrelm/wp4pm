<?php

class PositionOccupation extends Eloquent {

    protected $fillable = ['from', 'to', 'politician_id', 'position_id'];

    public $timestamps = false;

    public function politician()
    {
        return $this->belongsTo('Politician');
    }

    public function position()
    {
        return $this->belongsTo('Position');
    }

}