<?php

class Quote extends Eloquent {
        
    protected $fillable = ['text', 'semantic_score', 'community_score', 'status', 'article_id'];

    public function article()
    {
        return $this->belongsTo('Article');
    }

    public function fields()
    {
        return $this->belongsToMany('FieldTag', 'ner_fields');
    }

    public function parties()
    {
        return $this->belongsToMany('PartyTag', 'ner_parties');
    }

    public function politicians()
    {
        return $this->belongsToMany('PoliticianTag', 'ner_politicians');
    }

    public function typeOfPositions()
    {
        return $this->belongsToMany('TypeOfPositionTag', 'ner_type_of_positions');
    }

}
