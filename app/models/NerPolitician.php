<?php

class NerPolitician extends Eloquent {
        
    protected $fillable = ['quote_id', 'politician_tag_id'];

    public $timestamps = false;

    public function quote()
    {
        return $this->belongsTo('Quote');
    }

    public function politicianTag()
    {
        return $this->belongsTo('PoliticianTag');
    }

}
