<?php

class FieldTag extends Eloquent {
        
    protected $fillable = ['tag', 'field_id'];

    public $timestamps = false;

    public function field()
    {
        return $this->belongsTo('Field');
    }
    
    public function quotes()
    {
        return $this->belongsToMany('Quote', 'ner_fields');
    }

}
