<?php

class Position extends Eloquent {

    protected $fillable = ['party_id', 'type_of_position_id'];

    public $timestamps = false;

    public function party()
    {
        return $this->belongsTo('Party');
    }

    public function typeOfPosition()
    {
        return $this->belongsTo('TypeOfPosition');
    }

} 