<?php

class TypeOfPosition extends Eloquent {

    protected $fillable = ['name'];

    public $timestamps = false;

    public function tags()
    {
        return $this->hasMany('TypeOfPositionTag');
    }

} 