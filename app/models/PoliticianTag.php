<?php

class PoliticianTag extends Eloquent {
        
    protected $fillable = ['tag', 'politician_id'];

    public $timestamps = false;

    public function politician()
    {
        return $this->belongsTo('Politician');
    }

    public function quotes()
    {
        return $this->belongsToMany('Quote', 'ner_politicians');
    }

}
