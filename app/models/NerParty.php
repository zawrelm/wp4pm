<?php

class NerParty extends Eloquent {
        
    protected $fillable = ['quote_id', 'party_tag_id'];

    public $timestamps = false;

    public function quote()
    {
        return $this->belongsTo('Quote');
    }

    public function partyTag()
    {
        return $this->belongsTo('PartyTag');
    }

}
