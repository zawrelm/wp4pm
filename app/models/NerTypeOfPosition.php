<?php

class NerTypeOfPosition extends Eloquent {
        
    protected $fillable = ['quote_id', 'type_of_position_tag_id'];

    public $timestamps = false;

    public function quote()
    {
        return $this->belongsTo('Quote');
    }

    public function typeOfPositionTag()
    {
        return $this->belongsTo('TypeOfPositionTag');
    }

}
