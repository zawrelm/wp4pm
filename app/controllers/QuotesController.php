<?php

class QuotesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $quotes = Quote::orderBy('id', 'DESC')->take(100)->get();
        return View::make('quotes.list', ['quotes' => $quotes]);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $quote = Quote::find($id);
        return View::make('quotes.details', ['quote' => $quote]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$quote = Quote::find($id);
        $quote->fill(Input::all());

		/* Updates counters for the semantic score calculation */
		if($quote->status == 2 || $quote->status == -2)		//If a status for the Quote has been selected...
		{
			$parts = explode('"', $quote->text);
			$words = array();

            for($i = 1; $i < count($parts); $i += 2)
            {
                $words = explode(' ', $parts[$i]);		//... collect all the words in the direct quote parts
            }

			for($i = 0; $i < count($words); $i++)
			{
				$word = OntologyWord::where('word', $words[$i])->first();	//... query OntologyWord-entry from database

				if($word == null)							//If it doesn't exist, create entry and set counters
				{
					try {
						$q_acc = 0;
						$q_rej = 1;
						if($quote->status == 2) {
							$q_acc = 1;
							$q_rej = 0;
						}
						$new_word = \OntologyWord::create([
							'word' => $words[$i],
							'quotes_accepted' => $q_acc,
							'quotes_rejected' => $q_rej
						]);
					} catch(QueryException $e) { }
				}
				else
				{
					if($quote->status == 2) $word->quotes_accepted++;			//If accepted, increase word's accepted-counter by 1
					if($quote->status == -2) $word->quotes_rejected++;			//If rejected, increase word's rejected-counter by 1
					$word->save();
				}
			}
			
			$source = $quote->article->source;
			if($quote->status == 2) $source->accepted_quotes++;		//If accepted, increase source's accepted_quotes-counter by 1
			if($quote->status == -2) $source->rejected_quotes++;	//If rejected, increase source's rejected_quotes-counter by 1
			$source->save();
		}
				
		/* Store changes in Quote */
        $quote->save();
        
		/* Set Article's status */
		if($quote->status == 2) 
		{
			$quote->article->processed = 4;
			$quote->article->save();
		}
		else if($quote->article->processed == 2 || $quote->article->processed == 3)
		{
			$a_quotes = Quote::where('article_id', '=', $quote->article_id)->where('status', '>', -1);
			if(!$a_quotes->count())
			{
				$quote->article->processed = -4;
				$quote->article->save();
			}
		}
		
		/* Redirect to Quote list */
        return Redirect::to('quotes');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
