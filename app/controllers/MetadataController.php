<?php

class MetadataController extends \BaseController {

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $quote = Quote::find($id);
        $fields = Field::all();
        $politicians = Politician::orderBy('lastname')->get();
        $typeOfPositions = TypeOfPosition::all();
        $parties = Party::all();
        return View::make('quotes.metadata.general', ['quote' => $quote, 'fields' => $fields, 'politicians' => $politicians, 
                            'typeOfPositions' => $typeOfPositions, 'parties' => $parties]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function update($quote_id)
	{
        $quote = Quote::find($quote_id);

		/* Update Political Field of Quote */
        $field_ids = Input::get('field_id');
		if(is_array($field_ids))
		{
			$quote->fields()->detach();
			foreach($field_ids as $field_id)
			{
				$nerField = new NerField;
				$nerField->quote_id = $quote_id;
				$nerField->field_tag_id = Field::find($field_id)->tags->first()->id;
				$nerField->save();
			}
		}

		/* Update Speaker of Quote */
        $politician_id = Input::get('politician_id');
        if($politician_id != "")
		{
			$quote->politicians()->detach();
			
			$nerPolitician = new NerPolitician;
			$nerPolitician->quote_id = $quote_id;
			$nerPolitician->politician_tag_id = Politician::find($politician_id)->tags->first()->id;
			$nerPolitician->save();
		}

		/* Update Political Position of Speaker */
		$typeOfPosition_id = Input::get('type_of_position_id');
        if($typeOfPosition_id != "")
		{
			$quote->typeOfPositions()->detach();

			$nerTypeOfPosition = new NerTypeOfPosition;
			$nerTypeOfPosition->quote_id = $quote_id;
			$nerTypeOfPosition->type_of_position_tag_id = TypeOfPosition::find($typeOfPosition_id)->tags->first()->id;
			$nerTypeOfPosition->save();
		}
			
		/* Update Political Party of Speaker */
		$party_id = Input::get('party_id');
        if($party_id != "")
		{
			$quote->parties()->detach();
		
			$nerParty = new NerParty;
			$nerParty->quote_id = $quote_id;
			$nerParty->party_tag_id = Party::find($party_id)->tags->first()->id;
			$nerParty->save();
		}

        return Redirect::to('quotes/' . $quote_id);        
	}


	/**
	 * Show the form for creating a new resource.
	 *
     * @param  int  $id
	 * @return Response
	 */
	public function createField($id)
	{
        $quote = Quote::find($id);
        return View::make('quotes.metadata.field', ['quote' => $quote]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
     * @param  int  $id
	 * @return Response
	 */
	public function createPolitician($id)
	{
        $quote = Quote::find($id);
        return View::make('quotes.metadata.politician', ['quote' => $quote]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
     * @param  int  $id
	 * @return Response
	 */
	public function createTypeOfPosition($id)
	{
        $quote = Quote::find($id);
        return View::make('quotes.metadata.typeOfPosition', ['quote' => $quote]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
     * @param  int  $id
	 * @return Response
	 */
	public function createParty($id)
	{
        $quote = Quote::find($id);
        return View::make('quotes.metadata.party', ['quote' => $quote]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
     * @param  int  $id
	 * @return Response
	 */
	public function storeParty($id)
	{
		$party = new Party;
        $party->name = Input::get('name');
        $party->shortname = Input::get('shortname');
        $party->name = Input::get('link');
        $party->save();
        
        foreach(Input::get('tags') as $tag)
        {
            $partyTag = new PartyTag;
            $partyTag->tag = $tag;
            $partyTag->party_id = $party->id;
            $partyTag->save();
        }
        
        return Redirect::to('quotes/' . $id . '/metadata/general');
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
