<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*Route::get('/hello', function()
{
    $allPosts = Post::all()->count();

	return View::make('hello', ['count' => $allPosts]);
});*/

Route::get('quotes', 'QuotesController@index');
Route::get('quotes/{id}', 'QuotesController@edit');
Route::put('quotes/{id}', 'QuotesController@update');

Route::get('quotes/{id}/metadata/general', 'MetadataController@edit');
Route::post('quotes/{id}/metadata/general', 'MetadataController@update');

Route::get('quotes/{id}/metadata/field', 'MetadataController@createField');
Route::get('quotes/{id}/metadata/politician', 'MetadataController@createPolitician');
Route::get('quotes/{id}/metadata/party', 'MetadataController@createParty');
Route::get('quotes/{id}/metadata/position', 'MetadataController@createPosition');

Route::post('quotes/{id}/metadata/field', 'MetadataController@storeField');
Route::post('quotes/{id}/metadata/politician', 'MetadataController@storePolitician');
Route::post('quotes/{id}/metadata/party', 'MetadataController@storeParty');
Route::post('quotes/{id}/metadata/position', 'MetadataController@storePosition');
